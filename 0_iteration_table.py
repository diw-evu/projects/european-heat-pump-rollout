#%% Load packages

import os
import pandas as pd
import numpy as np

#%% Import

it_table= pd.read_csv(os.path.join("project_files","iterationfiles","iteration_table.csv"))

# Write run number

it_table["run"] = range(1,it_table.shape[0]+1)

#%% Export

it_table.to_csv(os.path.join("project_files","iterationfiles","iteration_table.csv"),index=None)
