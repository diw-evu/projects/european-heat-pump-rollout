
#%%

import sqlite3
import pandas as pd

#%%

# Read sqlite query results into a pandas DataFrame
con = sqlite3.connect("data/heat/when2heat_stacked.sqlite")

#%%

df1 = pd.read_sql_query("SELECT n from data", con)
df2 = pd.read_sql_query("SELECT year from data", con)
df3 = pd.read_sql_query("SELECT h from when2heat", con)
df4 = pd.read_sql_query("SELECT var from data", con)
df5 = pd.read_sql_query("SELECT att from data", con)

#%% extract unique values

countries  = pd.DataFrame( {'countries':  df1.country.unique()}       )
years      = pd.DataFrame( {'years':      df2.year.unique()}          )
variables  = pd.DataFrame( {'variables':  df2.variable.unique()}      )
attributes = pd.DataFrame( {'attributes': df3.attribute.unique()}     )
time       = pd.DataFrame( {'time':       df4.utc_timestamp.unique()} )

#%% write to sql

countries.to_sql('countries',   con, if_exists='replace', index=False)
variables.to_sql('variables',   con, if_exists='replace', index=False)
attributes.to_sql('attributes', con, if_exists='replace', index=False)
time.to_sql('time',             con, if_exists='replace', index=False)

#%% better when2heat data structure

df_main = pd.read_sql_query("SELECT * from when2heat", con)


#%%

# Verify that result of SQL query is stored in the dataframe
print(df.head())

print(df.country.unique())

con.close()

#%%

with open('data/heat/when2heat_stacked.sqlite', 'rb') as sql_file:
    sql_script = sql_file.read()

#%%
sqliteConnection = sqlite3.connect('whouse.db')
cursor = sqliteConnection.cursor()
cursor.executescript(sql_script)
cursor.close()