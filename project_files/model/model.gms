
********************************************************************************
$ontext
The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER).
Version 2.0.0, XX 2023.
Written by Alexander Zerrahn, Wolf-Peter Schill, and Fabian Stöckl.
This work is licensed under the MIT License (MIT).
For more information on this license, visit http://opensource.org/licenses/mit-license.php.
Whenever you use this code, please refer to http://www.diw.de/dieter.
We are happy to receive feedback under wschill@diw.de.
$offtext
********************************************************************************

Variables
Z                                Value objective function [Euro]
F(l,h)                           Energy flow over link l in hour h [MWh]
;

Positive Variables
G_L(n,tech,h)                    Generation level in hour h [MWh]
G_UP(n,tech,h)                   Generation upshift in hour h [MWh]
G_DO(n,tech,h)                   Generation downshift in hour h [MWh]
G_RES(n,tech,h)                  Generation renewables type res in hour h [MWh]

CU(n,tech,h)                     Renewables curtailment technology res in hour h [MWh]
* CU_sto(n,sto,h)                  Overflow of sto in hour h [MWh] (not implemented)

STO_IN(n,sto,h)                  Storage inflow technology sto hour h [MWh]
STO_OUT(n,sto,h)                 Storage outflow technology sto hour h [MWh]
STO_L(n,sto,h)                   Storage level technology sto hour h [MWh]
STO_L_INI_LAST(n,sto)            Storage: endogenous filling level in first and last hour [MWh]
        
EV_CHARGE(n,ev,h)                Electric vehicle charging vehicle profile ev hour h [MWh]
EV_DISCHARGE(n,ev,h)             Electric vehicle discharging vehicle profile ev hour h [MWh]
EV_L(n,ev,h)                     Electric vehicle charging level vehicle profile ev hour h [MWh]
EV_PHEVFUEL(n,ev,h)              Plug in hybrid electric vehicle conventional fuel use vehicle profile ev hour h [MWh]
EV_GED(n,ev,h)                   Grid electricity demand for mobility vehicle profile ev hour h [MWh]
        
N_TECH(n,tech)                   Technology tech built [MW]
N_STO_E(n,sto)                   Storage technology built - Energy [MWh]
N_STO_P_IN(n,sto)                Storage loading capacity built [MW]
N_STO_P_OUT(n,sto)               Storage discharging capacity built [MW]
        
DSM_CU(n,dsm,h)                  DSM: Load curtailment hour h [MWh]
DSM_UP(n,dsm,h)                  DSM: Load shifting up hour h technology dsm [MWh]
DSM_DO(n,dsm,h,hh)               DSM: Load shifting down in hour hh to account for upshifts in hour h technology dsm [MWh]
        
DSM_UP_DEMAND(n,dsm,h)           DSM: Load shifting up active for wholesale demand in hour h of technology dsm [MWh]
DSM_DO_DEMAND(n,dsm,h)           DSM: Load shifting down active for wholesale demand in hour h of technology dsm [MWh]
        
N_DSM_CU(n,dsm)                  DSM: Load curtailment capacity [MW]
N_DSM_SHIFT(n,dsm)               DSM: Load shifting capacity [MWh]

CU_PRO(n,tech,h)                 Prosumage: curtailment of renewable generation in hour h [MWh]
G_MARKET_PRO2M(n,tech,h)         Prosumage: energy sent to market in hour h [MWh]
G_MARKET_M2PRO(n,h)              Prosumage: withdrawal of energy from market in hour h [MWh]
G_RES_PRO(n,tech,h)              Prosumage: hourly renewables generation in hour h [MWh]
STO_IN_PRO2PRO(n,tech,sto,h)     Prosumage: storage loading from generation for discharging to consumption in hour h [MWh]
STO_IN_PRO2M(n,tech,sto,h)       Prosumage: storage loading from generation for discharging to market in hour h [MWh]
STO_IN_M2PRO(n,sto,h)            Prosumage: storage loading from market for discharging to consumption in hour h [MWh]
STO_IN_M2M(n,sto,h)              Prosumage: storage loading from market for discharging to market in hour h [MWh]
STO_OUT_PRO2PRO(n,sto,h)         Prosumage: storage discharging to consumption from generation in hour h [MWh]
STO_OUT_PRO2M(n,sto,h)           Prosumage: storage discharging to market from generation in hour h [MWh]
STO_OUT_M2PRO(n,sto,h)           Prosumage: storage discharging to consumption from market in hour h [MWh]
STO_OUT_M2M(n,sto,h)             Prosumage: storage discharging to market from market in hour h [MWh]
STO_L_PRO2PRO(n,sto,h)           Prosumage: storage level generation to consumption in hour h [MWh]
STO_L_PRO2M(n,sto,h)             Prosumage: storage level generation to market in hour h [MWh]
STO_L_M2PRO(n,sto,h)             Prosumage: storage level market to consumotion in hour h [MWh]
STO_L_M2M(n,sto,h)               Prosumage: storage level market to market in hour h [MWh]
N_STO_E_PRO(n,sto)               Prosumage: installed storage energy [MWh]
N_STO_P_PRO(n,sto)               Prosumage: installed storage power [MW]
STO_L_PRO(n,sto,h)               Prosumage: overall storage level in hour h [MWh]
N_RES_PRO(n,tech)                Prosumage: installed renewables capacities [MW]

NTC_ex(l)                        Trade: installed NTC export on line l [MW]
NTC_im(l)                        Trade: installed NTC import on line l [MW]

** new heating module

HEAT_ELEC_IN(n,heat_building,heat_sink,heat_tech,h)     Heating: ...
HEAT_STO_OUT(n,heat_building,heat_sink,heat_tech,h)     Heating: ...
HEAT_STO_IN(n,heat_building,heat_sink,heat_tech,h)      Heating: ...
HEAT_STO_LEV(n,heat_building,heat_sink,heat_tech,h)     Heating: ...
HEAT_STO_LEV_INI(n,heat_building,heat_sink,heat_tech)   Heating: ...

HEAT_N_STO_OUT(n,heat_building,heat_sink,heat_tech)     Heating: ...
HEAT_N_STO_IN(n,heat_building,heat_sink,heat_tech)      Heating: ...
HEAT_N_STO_E(n,heat_building,heat_sink,heat_tech)       Heating: ...
HEAT_N_ELEC_IN(n,heat_building,heat_sink,heat_tech)     Heating: ...

** old heating module
* H_DIR(n,bu,ch,h)                 Heating: direct heating in hour h for building type bu with haeting technology ch [MWh]
* H_SETS_LEV(n,bu,ch,h)            Heating: storage level SETS technologies [MWh]
* H_SETS_IN(n,bu,ch,h)             Heating: storage inflow SETS technologies [MWh]
* H_SETS_OUT(n,bu,ch,h)            Heating: storage outflow SETS technologies [MWh]
* H_HP_IN(n,bu,ch,hh)              Heating: electricity demand heat pump technologies [MWh]
* H_STO_LEV(n,bu,ch,h)             Heating: storage level storage technologies [MWh]
* H_STO_IN_HP(n,bu,ch,h)           Heating: storage inflow from heat pumps to storage technologies [MWh]
* H_STO_IN_ELECTRIC(n,bu,ch,h)     Heating: storage inflow from electric heating to storage technologies [MWh]
* H_ELECTRIC_IN(n,bu,ch,h)         Heating: hybrid electric heaters electricity demand [MWh]
* H_STO_IN_FOSSIL(n,bu,ch,h)       Heating: storage inflow from nonelectric heating to storage technologies [MWh]
* H_STO_OUT(n,bu,ch,h)             Heating: storage outflow from storage technologies [MWh]
* 
* H_DHW_DIR(n,bu,ch,h)             Heating - domestic hot water: provision in case of direct electric heating [MWh]
* H_DHW_STO_OUT(n,bu,ch,h)         Heating - domestic hot water: DHW storage outflow [MWh]
* 
* H_DHW_AUX_ELEC_IN(n,bu,ch,h)     Heating - domestic hot water: electrical energy input of auxiliary hot water tank for SETS [MWh]
* H_DHW_AUX_LEV(n,bu,ch,h)         Heating - domestic hot water: level of auxiliary hot water tank for SETS [MWh]
* H_DHW_AUX_OUT(n,bu,ch,h)         Heating - domestic hot water: auxiliary DHW provision for SETS [MWh]

** parsimonious P2H2P module
H2_ELY_IN(n,h2_ely,h)            Parsimonious H2: electrical energy input of electrolysis technology h2_ely in hour h [MWh_el]
H2_N_ELY(n,h2_ely)               Parsimonious H2: conversion capacity of electrolysis technology h2_ely [MW_el]

H2_STO_IN(n,h2_sto,h)            Parsimonious H2: H2 storage inflow of technology h2_sto in hour h [MWh_ch]
H2_STO_L(n,h2_sto,h)             Parsimonious H2: H2 storage level of technology h2_sto [MWh_ch]                              
H2_STO_OUT(n,h2_sto,h)           Parsimonious H2: H2 storage outflow of technology h2_sto in hour h [MWh_ch]
H2_STO_L_INI_LAST(n,h2_sto)      Parsimonious H2: endogenous filling level in first and last hour [MWh_ch]
H2_N_STO_E(n,h2_sto)             Parsimonious H2: energy capacity of H2 storage h2_sto [MWh_ch]

H2_N_RECON(n,h2_recon)           Parsimonious H2: reconversion capacity of technology h2_recon [MWh_el]
H2_RECON_OUT(n,h2_recon,h)       Parsimonious H2: electrical energy output of H2 reconversion technology h2_recon in hour h [MWh_el]

** extensive P2H2 module
H2_N_PROD_CENT(n,h2_tech)                                       P2H2 - centralized electrolysis capacity (in terms of electricity inflow)
H2_N_PROD_DECENT(n,h2_tech)                                     P2H2 - decentralized electrolysis capacity (in terms of electricity inflow)
H2_E_H2_IN(n,h2_tech,h2_channel,h)                              P2H2 - electricity consumption for electrolysis (in terms of electricity inflow)
H2_PROD_OUT(n,h2_tech,h2_channel,h)                             P2H2 - H2 flow out after electrolysis (kWh in terms of the lower heating value (LHV) of H2) (= inflow for the next process)

H2_N_PROD_AUX(n,h2_tech,h2_channel)                             P2H2 - capacity to bring H2 into the right "form" for distribution after electrolysis
H2_PROD_AUX_IN(n,h2_tech,h2_channel,h)                          P2H2 - H2 flow in for this process (no losses modelled)
H2_PROD_AUX_OUT(n,h2_channel,h)                                 P2H2 - H2 flow out for this process (as no losses: identical to flow in) (= inflow for the next process)

H2_N_HYD_LIQ(n,h2_channel)                                      P2H2 - H2 hydration capacity (in terms of H2 flow)
H2_HYD_LIQ_OUT(n,h2_channel,h)                                  P2H2 - H2 hydration flow out (no losses modelled) (= inflow for the next process)

H2_N_STO(n,h2_channel)                                          P2H2 - H2 production site storage capacity (in terms of H2) (potential losses during storage)
H2_STO_P_OUT(n,h2_channel,h)                                    P2H2 - H2 production site storage flow out for each hour (in terms of H2 flow) (= inflow for the next process)
H2_STO_P_IN(n,h2_channel,h)                                     P2H2 - H2 production site storage flow in for each hour (in terms of H2 flow)
H2_STO_P_L(n,h2_channel,h)                                      P2H2 - H2 production site storage filling level for each hour (in terms of H2)
H2_STO_P_L0(n,h2_channel)                                       P2H2 - H2 production site storage initial fillin attributed (in terms of H2)

H2_N_AUX_PRETRANS(n,h2_channel)                                 P2H2 - capacity to bring H2 into the right "form" for transportation after storage
H2_AUX_PRETRANS_OUT(n,h2_channel,h)                             P2H2 - H2 flow out (no losses modelled) (= inflow for the next process)

H2_N_TRANS(n,h2_channel)                                        P2H2 - H2 transporation capacity (in terms of H2 flow)
H2_N_AVAI_TRANS(n,h2_channel,h)                                 P2H2 - H2 available transporation capacity (in terms of H2 flow)
H2_TRANS_OUT(n,h2_channel,h)                                    P2H2 - H2 transportation flow out (no losses modelled) (= inflow for the next process)

H2_AUX_BFLP_STO_OUT(n,h2_channel,h)                             P2H2 - auxiliary process before LP storage outflow
H2_N_AUX_BFLP_STO(n,h2_channel)                                 P2H2 - auxiliary process before LP storage capacity

H2_N_LP_STO(n,h2_channel)                                       P2H2 - LP storage capacity (in terms of H2) (potential losses during storage)
H2_LP_STO_OUT(n,h2_channel,h)                                   P2H2 - LP storge out (no losses modelled) (= inflow for the next process) (in terms of H2 flow)
H2_LP_STO_L(n,h2_channel,h)                                     P2H2 - LP storage filling level for each hour (in terms of H2)
H2_LP_STO_L0(n,h2_channel)                                      P2H2 - LP storage initial filling attributed (in terms of H2)
H2_AUX_BFLP_STO_IN(n,h2_channel,h)                              P2H2 - auxiliary process before LP storge inflow (no losses modelled) (= inflow for the next process) (in terms of H2 flow)

H2_N_DEHYD_EVAP(n,h2_channel)                                   P2H2 - H2 dehydration capacity (in terms of H2 flow)
H2_DEHYD_EVAP_OUT(n,h2_channel,h)                               P2H2 - H2 dehydration flow out (no losses modelled) (= inflow for the next process)

H2_AUX_BFMP_STO_OUT(n,h2_channel,h)                             P2H2 - auxiliary process before MP storage outflow
H2_N_AUX_BFMP_STO(n,h2_channel)                                 P2H2 - auxiliary process before MP storage capacity

H2_N_MP_STO(n,h2_channel)                                       P2H2 - MP storage capacity (in terms of H2) (potential losses during storage)
H2_MP_STO_L(n,h2_channel,h)                                     P2H2 - MP storage filling level for each hour (in terms of H2)
H2_MP_STO_L0(n,h2_channel)                                      P2H2 - MP storage initial filling attributed (in terms of H2)
H2_MP_STO_OUT(n,h2_channel,h)                                   P2H2 - MP storge out (no losses modelled) (= inflow for the next process) (in terms of H2 flow)

H2_N_AUX_BFHP_STO(n,h2_channel)                                 P2H2 - auxiliary process before HP storage outflow
H2_AUX_BFHP_STO_OUT(n,h2_channel,h)                             P2H2 - auxiliary process before HP storage capacity
H2_AUX_BFHP_STO_IN(n,h2_channel,h)                              P2H2 - auxiliary process before HP storge inflow (no losses modelled) (= inflow for the next process) (in terms of H2 flow)

H2_N_HP_STO(n,h2_channel)                                       P2H2 - HP storage capacity (in terms of H2) (potential losses during storage)
H2_HP_STO_OUT(n,h2_channel,h)                                   P2H2 - HP storage flow out for each hour (in terms of H2 flow)
H2_HP_STO_L(n,h2_channel,h)                                     P2H2 - HP storage filling level for each hour (in terms of H2)
H2_HP_STO_L0(n,h2_channel)                                      P2H2 - HP storage part of initial filling attributed to a channel (in terms of H2)

H2_AUX_BFFUEL_OUT(n,h2_channel,h)                               P2H2 - auxiliary process before fueling outflow
H2_N_AUX_BFFUEL(n,h2_channel)                                   P2H2 - auxiliary process before fueling capacity

H2_BYPASS_1(n,h2_tech,h2_channel,h)                             P2H2 - bypass flow from production sites to auxiliary process before transportation
H2_BYPASS_2(n,h2_channel,h)                                     P2H2 - bypass flow from unloading to auxiliary process before HP storage

H2_N_RECON_AUX(n,h2_channel,h2_tech_recon)                      P2H2 - capacity to bring H2 into the right "form" for reconversion
H2_RECON_AUX_OUT(n,h2_channel,h2_tech_recon,h)                  P2H2 - H2 flow in for this process (no losses modelled)

H2_N_RECON_extensive(n,h2_tech_recon)                           P2H2 - re-conversion capacity (in terms of H2 inflow)
H2_E_RECON_OUT(n,h2_channel,h2_tech_recon,h)                    P2H2 - electricity generation at re-conversion (in terms of electricity outflow)

H2_CHANNEL_SHARE(n,h2_channel)                                  P2H2 - share of filling stations supplied by either channel (only one channel per filling station allowed)

G_INFES(n,h)
*H_INFES(n,bu,ch,h)
*H_DHW_INFES(n,bu,ch,h)
;

********************************************************************************

Equations
* Objective
obj                                                         Objective cost minimization

* Energy balance
con1a_bal(n,h)                                              Energy Balance

* Load change costs
con2a_loadlevel(n,tech,h)                                   Load change costs: Level
con2b_loadlevelstart(n,tech,h)                              Load change costs: Level for first period

* Capacity contraints and flexibility constraints
con3a_maxprod_dispatchable(n,tech,h)                        Capacity Constraint conventionals
con3b_maxprod_res(n,tech,h)                                 Capacity constraints renewables

* Storage constraints
con4a1_stolev(n,sto,h)                                      Storage Level Dynamics: general
con4a2_stolev_ini(n,sto,h)                                  Storage Level Dynamics: first hour
con4a3_stolev_last(n,sto,h)                                 Storage Level Dynamics: last hour
con4b_stolev_max(n,sto,h)                                   Storage energy maximum
con4c_stolev_min(n,sto,h)                                   Storage energy minimum
con4d_maxin_sto(n,sto,h)                                    Storage inflow maximum - not more than installed capacity
con4e_maxout_sto(n,sto,h)                                   Storage outflow maximum - not more than installed capacity
con4f_maxin_lev(n,sto,h)                                    Storage inflow maximum - not more than energy capacity minus level of last period
con4g_maxout_lev(n,sto,h)                                   Storage outflow maximum - not more than level of last period
con4h_EtoP(n,sto)                                           Maximum E to P ratio

* Minimum restrictions for renewables and biomass
con5a_minRES(n)                                             Minimum yearly renewables requirement
con5b_max_energy(n,tech)                                    Maximum yearly biomass energy

* DSM conditions: Load curtailment
con6a_DSMcurt_duration_max(n,dsm,h)                         Maximum curtailment energy budget per time
con6b_DSMcurt_max(n,dsm,h)                                  Maximum curtailment per period

* DSM conditions: Load shifting
con7a_DSMshift_upanddown(n,dsm,h)                           Equalization of upshifts and downshifts in due time
con7b_DSMshift_granular_max(n,dsm,h)                        Maximum shifting in either direction per period
con7c_DSM_distrib_up(n,dsm,h)                               Distribution of upshifts between wholesale and reserves
con7d_DSM_distrib_do(n,dsm,h)                               Distribution of downshifts between wholesale and reserves
con7e_DSMshift_recovery(n,dsm,h)                            Recovery times

* Minimum/maximum installation conditions
con8a1_min_I_power(n,tech)                                  Minimum installable capacities: electricity generation
con8a2_max_I_power(n,tech)                                  Maximum installable capacities: electricity generation
con8b1_min_I_sto_e(n,sto)                                   Minimum installable capacities: storage energy
con8b2_max_I_sto_e(n,sto)                                   Maximum installable capacities: storage energy
con8c1_min_I_sto_p_in(n,sto)                                Minimum installable capacities: storage power charging
con8c2_max_I_sto_p_in(n,sto)                                Maximum installable capacities: storage power charging
con8d1_min_I_sto_p_out(n,sto)                               Minimum installable capacities: storage power discharging
con8d2_max_I_sto_p_out(n,sto)                               Maximum installable capacities: storage power discharging
con8e_max_I_dsm_cu(n,dsm)                                   Maximum installable capacities: DSM curtailment
con8f_max_I_dsm_shift_pos(n,dsm)                            Maximum installable capacities: DSM shifting
con8g_max_pro_res(n,tech)                                   Maximum installable capacities: renewables prosumagers
con8h_max_pro_sto_e(n,sto)                                  Maximum installable capacities: storage energy prosumagers
con8i_max_sto_pro_p(n,sto)                                  Maximum installable capacities: storage power prosumagers
con8j1a_min_I_ntc_ex(l)                                     Minimum installable capacities: NTC export
con8j1b_max_I_ntc_ex(l)                                     Maximum installable capacities: NTC export
con8j2a_min_I_ntc_im(l)                                     Minimum installable capacities: NTC import
con8j2b_max_I_ntc_im(l)                                     Maximum installable capacities: NTC import
con8k1_min_I_h2_ely_p(n,h2_ely)                             Minimum installable capacities: H2 electrolysis
con8k2_max_I_h2_ely_p(n,h2_ely)                             Maximum installable capacities: H2 electrolysis
con8l1_min_I_h2_sto_e(n,h2_sto)                             Minimum installable capacities: H2 storage energy
con8l2_max_I_h2_sto_e(n,h2_sto)                             Maximum installable capacities: H2 storage energy
con8m1_min_I_h2_recon_p(n,h2_recon)                         Minimum installable capacities: H2 reconversion
con8m2_max_I_h2_recon_p(n,h2_recon)                         Maximum installable capacities: H2 reconversion                       

** parsimonious P2H2P module
con9a_h2_ely_conv(n,h2_ely,h2_sto,h)                        H2 generation using electrolysis
con9b_h2_ely_conv_max(n,h2_ely,h)                           Maximum H2 generation using electrolysis
con9c_h2_sto_lev_start(n,h2_sto,h)                          H2 storage level law of motion - initial condition for first hour
con9d_h2_sto_lev(n,h2_sto,h)                                H2 storage level law of motion - general condition
con9e_h2_sto_lev_end(n,h2_sto,h)                            H2 storage level law of motion - end condition for last hour
con9f_h2_sto_lev_max(n,h2_sto,h)                            Maximum H2 storage energy level
con9g_h2_sto_lev_min(n,h2_sto,h)                            Minimum H2 storage energy level
con9h_h2_recon(n,h2_recon,h2_sto,h)                         H2 reconversion to electric energy
con9i_h2_recon_max(n,h2_recon,h)                            Maximum H2 reconversion to electric energy

* Electric vehicles
con10a_ev_ed(n,ev,h)                                        Energy balance of electric vehicles
con10b_ev_chargelev_start(ev,h,n)                           Cumulative charging level in the first hour
con10c_ev_chargelev(ev,h,n)                                 Cumulative charging level in hour h
con10d_ev_chargelev_max(n,ev,h)                             Cumulative maximal charging level
con10e_ev_maxin(n,ev,h)                                     Cumulative maximal charging power
con10f_ev_maxout(n,ev,h)                                    Cumulative maximal discharging power
con10g_ev_chargelev_ending(n,ev,h)                          Cumulative charging level in the last hour
con10h_ev_maxin_lev(n,ev,h)                                 Cumulative maximal charging limit
con10i_ev_maxout_lev(n,ev,h)                                Cumulative maximal discharging limit
con10k_ev_exog(n,ev,h)                                      Exogenous EV charging

* Prosumage
con11a_pro_distrib(n,tech,h)                                Prosumage: distribution of generated energy
con11b_pro_balance(n,h)                                     Prosumage: energy balance
con11c_pro_selfcon(n)                                       Prosumage: minimum self-generation requirement
con11d_pro_stolev_PRO2PRO(n,sto,h)                          Prosumage: storage level prosumager-to-prosumagers
con11e_pro_stolev_PRO2M(n,sto,h)                            Prosumage: storage level prosumagers-to-market
con11f_pro_stolev_M2PRO(n,sto,h)                            Prosumage: storage level market-to-prosumagers
con11g_pro_stolev_M2M(n,sto,h)                              Prosumage: storage level market-to-market
con11h_1_pro_stolev_start_PRO2PRO(n,sto,h)                  Prosumage: storage level initial conditions
con11h_2_pro_stolev_start_PRO2M(n,sto,h)                    Prosumage: storage level initial conditions
con11h_3_pro_stolev_start_M2PRO(n,sto,h)                    Prosumage: storage level initial conditions
con11h_4_pro_stolev_start_M2M(n,sto,h)                      Prosumage: storage level initial conditions
con11i_pro_stolev(n,sto,h)                                  Prosumage: storage level total
con11j_pro_stolev_max(n,sto,h)                              Prosumage: maximum overall storage level
con11k_pro_maxin_sto(n,sto,h)                               Prosumage: maximum storage inflow
con11l_pro_maxout_sto(n,sto,h)                              Prosumage: maximum storage outflow
con11m_pro_maxout_lev(n,sto,h)                              Prosumage: maximum storage outflow linked to level
con11n_pro_maxin_lev(n,sto,h)                               Prosumage: maximum storage inflow linked to level
con11o_pro_ending(n,sto,h)                                  Prosumage: storage ending condition

* Cross-nodal trade
con12a_max_f(l,h)                                           Maximum energy flow limited to positive NTC
con12b_min_f(l,h)                                           Minimum energy flow limited to negative NTC

* Residential heat (new module)
con_heat_bal(n,heat_building,heat_sink,heat_tech,h)          Heating: ...
con_heat_sto_lev(n,heat_building,heat_sink,heat_tech,h)      Heating: ...
con_heat_sto_lev_ini(n,heat_building,heat_sink,heat_tech,h)  Heating: ...
con_heat_sto_lev_end(n,heat_building,heat_sink,heat_tech,h)  Heating: ...
con_heat_sto_lev_max(n,heat_building,heat_sink,heat_tech,h)  Heating: ...
con_heat_elec_in(n,heat_building,heat_sink,heat_tech,h)      Heating: ...
con_heat_elec_in_max(n,heat_building,heat_sink,heat_tech,h)  Heating: ...
con_heat_n_sto_out(n,heat_building,heat_sink,heat_tech)      Heating: ...
con_heat_n_sto_in(n,heat_building,heat_sink,heat_tech)       Heating: ...
con_heat_n_sto_e(n,heat_building,heat_sink,heat_tech)        Heating: ...
con_heat_n_elec_in(n,heat_building,heat_sink,heat_tech)      Heating: ...

* Residential heat (old module)
*con14a_heat_balance(n,bu,ch,h)                              Space heating energy balance
*con14b_dhw_balance(n,bu,ch,h)                               Domestic hot water energy balance
*con14c_sets_level(n,bu,ch,h)                                SETS - level law of motion
*con14d_sets_level_start(n,bu,ch,h)                          SETS - storage level initial condition
*con14e_sets_maxin(n,bu,ch,h)                                SETS - maximum energy inflow
*con14f_sets_maxout(n,bu,ch,h)                               SETS - maximum energy outflow
*con14h_sets_maxlev(n,bu,ch,h)                               SETS - maximum storage level
*con14i_sets_aux_dhw_level(n,bu,ch,h)                        SETS auxiliary DHW module - storage level law of motion
*con14j_sets_aux_dhw_level_start(n,bu,ch,h)                  SETS auxiliary DHW module - storage level initial consition
*con14k_sets_aux_dhw_maxin(n,bu,ch,h)                        SETS auxiliary DHW module - maximum energy inflow
*con14m_sets_aux_dhw_maxlev(n,bu,ch,h)                       SETS auxiliary DHW module - maximum storage level
*con14n_hp_in(n,bu,ch,h)                                     Heat pumps - electricity demand
*con14o_hp_maxin(n,bu,ch,h)                                  Heat pumps - maximum electricity demand
*con14q_storage_elec_in(n,bu,ch,h)                           Hybrid electric heating - electricity demand
*con14r_storage_elec_maxin(n,bu,ch,h)                        Hybrid electric heating - maximum electricity demand
*con14t_storage_level(n,bu,ch,h)                             Storage heating - level law of motion
*con14u_storage_level_start(n,bu,ch,h)                       Hybrid electric heating - storage level initial condition
*con14v_storage_maxlev(n,bu,ch,h)                            Hybrid electric heating - maximum storage level

** extensive P2H2 module

* Electrolysis_Centers
h2_con2a(n,h2_tech,h2_tech_recon,h)                         Capacity of centralised electrolysis
h2_con2b(n,h2_tech,h)                                       Capacity of decentralised electrolysis
h2_con3(n,h2_tech,h2_channel,h)                             Hydrogen outflow from electrolysis (Bypass1)

* aux_prod_site
h2_con4(n,h2_tech,h2_channel,h)                             Production auxiliaries capacity
h2_con5(n,h2_tech,h2_channel,h)                             Electrolysis Hydrogen outflow bypassed
h2_con6(n,h2_channel,h)                                     Production aixiliaries hydrogen outflow

* hydration_liquefaction
h2_con7(n,h2_channel,h)                                     Hydrogenation and liquefaction outflow
h2_con8(n,h2_channel,h)                                     Hydrogenation and liquefaction Capacity

* prod_site_storage
h2_con9(n,h2_channel,h)                                     Hydrogen inflow of the storage at productione sites
h2_con10(n,h2_channel,h)                                    Capacity of the storage at production sites
h2_con11a(n,h2_channel,h)                                   Filling level of the storage at production sites
h2_con11b(n,h2_channel,h)                                   Initial filling level of the storage at production sites
h2_con11c(n,h2_channel)                                     Minimum of initial filling level of the storage at production sites
h2_con11d(n,h2_channel,h)                                   Initial filling level of the storage at production sites equal to that of the last one
h2_con11e(n,h2_channel,h)                                   Minimum of filling level of the storage at production sites

* aux_bftrans
h2_con12(n,h2_channel,h)                                    Outflow of the auxiliary process before transportation (bypass 1)
h2_con13(n,h2_channel,h)                                    Capacity of the auxiliary process before transportation
h2_con13b(n,h2_channel,h)                                   Capacity of filling stations for handling hydrogen outflow of the auxiliary process before transportation

* transportation
h2_con14a(n,h2_channel,h)                                   Hydrogen outflow of transportation (simplified form)
h2_con14b(n,h2_channel,h)                                   Capacity of transportation (simplified form)
h2_con14c(n,h2_channel,h)                                   Hydrogen outflow of transportation (time-consuming form) (delivery in the same year)

h2_con15a(n,h2_channel,h)                                   Hydrogen outflow of transportation (time-consuming form)
h2_con15b(n,h2_channel,h)                                   Hydrogen outflow of transportation (re-conversion channel)
h2_con114(n,h2_channel,h)                                   Available capacity of transportation (time-consuming form)
h2_con115(n,h2_channel,h)                                   Total capacity of transportation (time-consuming form)

* aux_bflp_storage
h2_con_1(n,h2_channel,h)                                    Hydrogen outflow of the auxiliary process before LP storage
h2_con_2(n,h2_channel,h)                                    Capacity of the auxiliary process before LP storage
h2_con17e(n,h2_channel,h)                                   Hydrogen inflow of the auxiliary process before LP storage (bypass 2)

* LP_storage
h2_con16a(n,h2_channel,h)                                   Capacity of the LP storage
h2_con16b(n,h2_channel,h)                                   Upper bound of the capacity of the LP storage (filling stations)
h2_con16c(n,h2_channel,h)                                   Upper bound of the capacity of the LP storage (re-conversion channel)
h2_con17a(n,h2_channel,h)                                   Filling level of the LP storage
h2_con17b(n,h2_channel,h)                                   Initial filling level of the LP storage
h2_con17c(n,h2_channel)                                     Minimum of initial filling level of the LP storage
h2_con17d(n,h2_channel,h)                                   Initial filling level of the LP storage equal to that of the last one
h2_con17g(n,h2_channel,h)                                   Minimum of filling level of the LP storage

* dehydration_evaporation
h2_con18(n,h2_channel,h)                                    Outflow of dehydration and evaporation
h2_con19(n,h2_channel,h)                                    Capacity of dehydration and evaporation

* aux_bfMP_storage
h2_con_7(n,h2_channel,h)                                    Outflow of  the auxiliary process before MP storage
h2_con_8(n,h2_channel,h)                                    Capacity of the auxiliary process before MP storage

* MP_storage
h2_con_3(n,h2_channel,h)                                    Capacity of the MP storage
h2_con_4a(n,h2_channel,h)                                   Filling level of the MP storage
h2_con_4b(n,h2_channel,h)                                   Initial filling level of the MP storage
h2_con_4c(n,h2_channel)                                     Minimum of initial filling level of the LP storage
h2_con_4d(n,h2_channel,h)                                   Initial filling level of the MP storage equal to that of the last one
h2_con_4e(n,h2_channel,h)                                   Minimum of filling level of the MP storage
h2_con_4f(n,h2_channel,h)                                   Upper bound of the capacity of the MP storage (filling stations)
h2_con_4g(n,h2_channel,h)                                   Upper bound of the capacity of the MP storage (re-conversion channel)


* aux_bfhp_storage
h2_con21(n,h2_channel,h)                                    Hydrogen outflow of the auxiliary process before HP storage
h2_con21b(n,h2_channel,h)                                   Hydrogen inflow of the auxiliary process before HP storage (bypass 2)
h2_con22(n,h2_channel,h)                                    Capacity of the auxiliary process before HP storage

* hp_storage
h2_con24(n,h2_channel,h)                                    Capacity of the HP storage
h2_con25a(n,h2_channel,h)                                   Filling level of the HP storage
h2_con25b(n,h2_channel,h)                                   Initial filling level of the HP storage
h2_con25c(n,h2_channel,h)                                   Initial filling level of the HP storage equal to that of the last one
h2_con25d(n,h2_channel,h)                                   Min. of filling level of the HP storage
h2_con25e(n,h2_channel,h)                                   Upper bound of the capacity of the HP storage (filling stations)
h2_con25f(n,h2_channel,h)                                   Upper bound of the capacity of the HP storage (re-conversion channel)

* aux_bffuel
h2_con_5(n,h2_channel,h)                                    Outflow of the auxiliary process before fueling
h2_con_6(n,h2_channel,h)                                    Capacity of the auxiliary process before MP storage

* matching demand and supply
h2_con28a(n,h,h2_channel)                                   Demand of the filling stations for hydrogen
h2_con28b(n)                                                Sum of shares of all channels
h2_con29(n,h,h2_channel)                                    Demand matching for p2x other than H2

* aux_recon_site
h2_con31(n,h2_channel,h2_tech_recon,h)                      Capacity of the auxliary process  before re-conversion
h2_con32(n,h2_channel,h)                                    Distribution of the outflow of the auxiliary process before re-conversion

* recon
h2_con33(n,h2_tech_recon,h)                                 Capacity of the re-conversion process
h2_con34(n,h2_channel,h2_tech_recon,h)                      Electricity generation of the re-conversion process

%DIETERgms%$include "%MODELDIR%dieterpy_3_definenewconstr.gms"

;

********************************************************************************

* ---------------------------------------------------------------------------- *
***** Objective function *****
* ---------------------------------------------------------------------------- *

* c_fuel(n,tech)$eta(n,tech) = fuelprice(n,tech)/eta(n,tech) ;
* c_co2(n,tech)$eta(n,tech) = carbon_content(n,tech)/eta(n,tech)*CO2price(n,tech);
* c_m(n,tech)$eta(n,tech) = c_vom(n,tech) + c_fuel(n,tech) + c_co2(n,tech) ;

obj..
        Z =E=
*                  sum( (map_n_tech(n,dis),h) ,            c_m(n,dis)        * G_L(n,dis,h) )
                  sum( (map_n_tech(n,dis),h) ,( c_vom(n,dis) + fuelprice(n,dis)/eta(n,dis) + carbon_content(n,dis)/eta(n,dis)*CO2price(n,dis) ) * G_L(n,dis,h) )
                + sum( (map_n_tech(n,dis),h)$(ord(h)>1) , c_up(n,dis)       * G_UP(n,dis,h) )
                + sum( (map_n_tech(n,dis),h)$(ord(h)>1) , c_do(n,dis)       * G_DO(n,dis,h) )
                + sum( (map_n_tech(n,nondis),h) ,         c_cu(n,nondis)    * CU(n,nondis,h)  + c_vom(n,nondis)    * G_RES(n,nondis,h) )
                + sum( (map_n_sto(n,sto),h) ,             c_m_sto_in(n,sto) * STO_IN(n,sto,h) + c_m_sto_out(n,sto) * STO_OUT(n,sto,h) )
%DSM%$ontext
                + sum( (map_n_dsm(n,dsm_curt),h) ,  c_m_dsm_cu(n,dsm_curt)     * DSM_CU(n,dsm_curt,h) )
                + sum( (map_n_dsm(n,dsm_shift),h) , c_m_dsm_shift(n,dsm_shift) * DSM_UP_DEMAND(n,dsm_shift,h) )
                + sum( (map_n_dsm(n,dsm_shift),h) , c_m_dsm_shift(n,dsm_shift) * DSM_DO_DEMAND(n,dsm_shift,h) )
$ontext
$offtext
%EV%$ontext
                + sum( (map_n_ev(n,ev),h) , c_m_ev_cha(n,ev)   * EV_CHARGE(n,ev,h) )
                + sum( (map_n_ev(n,ev),h) , c_m_ev_dis(n,ev)   * EV_DISCHARGE(n,ev,h) )
                + sum( (map_n_ev(n,ev),h) , pen_phevfuel(n,ev) * EV_PHEVFUEL(n,ev,h) )
$ontext
$offtext
                + sum( map_n_tech(n,tech) , c_i(n,tech)      * N_TECH(n,tech) )
                + sum( map_n_tech(n,tech) , c_fix(n,tech)    * N_TECH(n,tech) )
                + sum( map_n_sto(n,sto)   , c_i_sto_e(n,sto) * N_STO_E(n,sto) + c_i_sto_p_in(n,sto)*N_STO_P_IN(n,sto) + c_i_sto_p_out(n,sto)*N_STO_P_OUT(n,sto) )
                + sum( map_n_sto(n,sto)   , c_fix_sto_p_in(n,sto)*N_STO_P_IN(n,sto) + c_fix_sto_p_out(n,sto)*N_STO_P_OUT(n,sto) + c_fix_sto_e(n,sto)*N_STO_E(n,sto) )
%DSM%$ontext
                + sum( map_n_dsm(n,dsm_curt)  , c_i_dsm_cu(n,dsm_curt)       * N_DSM_CU(n,dsm_curt) )
                + sum( map_n_dsm(n,dsm_curt)  , c_fix_dsm_cu(n,dsm_curt)     * N_DSM_CU(n,dsm_curt) )
                + sum( map_n_dsm(n,dsm_shift) , c_i_dsm_shift(n,dsm_shift)   * N_DSM_SHIFT(n,dsm_shift) )
                + sum( map_n_dsm(n,dsm_shift) , c_fix_dsm_shift(n,dsm_shift) * N_DSM_SHIFT(n,dsm_shift) )
$ontext
$offtext

%prosumage%$ontext
                + sum( map_n_res_pro(n,res) , c_i(n,res)*N_RES_PRO(n,res) )
                + sum( map_n_res_pro(n,res) , c_fix(n,res)*N_RES_PRO(n,res) )

                + sum( map_n_sto_pro(n,sto) , c_i_sto_e(n,sto)*N_STO_E_PRO(n,sto) )
                + sum( map_n_sto_pro(n,sto) , c_fix_sto_p_in(n,sto)*N_STO_P_PRO(n,sto) + c_fix_sto_e(n,sto) * N_STO_E_PRO(n,sto) )
                + sum( map_n_sto_pro(n,sto) , c_i_sto_p_in(n,sto)*N_STO_P_PRO(n,sto) )

                + sum( (map_n_sto_pro(n,sto),h) , c_m_sto_out(n,sto) * ( STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_M2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h) + STO_OUT_M2M(n,sto,h)) + c_m_sto_in(n,sto) * ( sum( res , STO_IN_PRO2PRO(n,res,sto,h) + STO_IN_PRO2M(n,res,sto,h)) + STO_IN_M2PRO(n,sto,h) + STO_IN_M2M(n,sto,h) ) )
$ontext
$offtext
                + sum(  map_l(l)    , (c_i_ntc(l) + c_fix_ntc(l)) * 0.5 * (NTC_ex(l) + NTC_im(l))  )
                + sum( (map_l(l),h) , c_m_ntc(l) * F(l,h) )

*%heat%$ontext
*                + sum( (n,bu,hfo,h) , pen_heat_fuel(n,bu,hfo) * H_STO_IN_FOSSIL(n,bu,hfo,h))
*$ontext
*$offtext
                + sum( (n,h) , c_infes * G_INFES(n,h) )
*%heat%$ontext
*                + sum( (n,bu,ch,h) , c_h_infes     * H_INFES(n,bu,ch,h) )
*                + sum( (n,bu,ch,h) , c_h_dhw_infes * H_DHW_INFES(n,bu,ch,h) )
*$ontext
*$offtext

%P2H2P_parsimonious%$ontext
* marginal costs for compression and reconversion
                + sum( (map_n_h2_ely(n,h2_ely),map_n_h2_sto(n,h2_sto),h), c_m_h2_comp(n,h2_ely) * H2_STO_IN(n,h2_sto,h) )
                + sum( (map_n_h2_recon(n,h2_recon),h), c_m_h2_recon(n,h2_recon) * H2_RECON_OUT(n,h2_recon,h) )
* electrolysis capex
                + sum( map_n_h2_ely(n,h2_ely), c_i_h2_ely(n,h2_ely) * H2_N_ELY(n,h2_ely) )
                + sum( map_n_h2_ely(n,h2_ely), c_fix_h2_ely(n,h2_ely) * H2_N_ELY(n,h2_ely) )
* H2 compression capex
                + sum( (map_n_h2_ely(n,h2_ely)), c_i_h2_comp(n,h2_ely) * ( eta_h2_ely(n,h2_ely) * H2_N_ELY(n,h2_ely)) ) 
                + sum( (map_n_h2_ely(n,h2_ely)), c_fix_h2_comp(n,h2_ely) * ( eta_h2_ely(n,h2_ely) * H2_N_ELY(n,h2_ely)) )
* H2 storage capex                
                + sum( map_n_h2_sto(n,h2_sto), c_i_h2_sto_e(n,h2_sto) * H2_N_STO_E(n,h2_sto) )
                + sum( map_n_h2_sto(n,h2_sto), c_fix_h2_sto_e(n,h2_sto) * H2_N_STO_E(n,h2_sto) )
* H2 reconversion capex
                + sum( map_n_h2_recon(n,h2_recon), c_i_h2_recon(n,h2_recon) * H2_N_RECON(n,h2_recon) )
                + sum( map_n_h2_recon(n,h2_recon), c_fix_h2_recon(n,h2_recon) * H2_N_RECON(n,h2_recon) )
$ontext
$offtext

%P2H2_extensive%$ontext

* prod
                + sum( (n,h2_tech) , ( h2_prod_c_ad_a_overnight(n,h2_tech) + h2_prod_c_ad_a_fix(n,h2_tech) + h2_prod_c_ad_a_fix2(n,h2_tech) ) * H2_N_PROD_CENT(n,h2_tech) )
                + sum( (n,h2_tech) , ( h2_prod_c_a_overnight(n,h2_tech) + h2_prod_c_a_fix(n,h2_tech) + h2_prod_c_a_fix2(n,h2_tech) ) * H2_N_PROD_DECENT(n,h2_tech) )
* aux_prod_site
                + sum( (n,h2_tech,h2_channel) , h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_c_a_overnight(n,h2_tech,h2_channel) * H2_N_PROD_AUX(n,h2_tech,h2_channel) )
* hydration_liquefaction
                + sum( (n,h2_channel) , h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_c_a_overnight(n,h2_channel) * H2_N_HYD_LIQ(n,h2_channel) )
                + sum( (n,h2_channel,h) , h2_hyd_liq_sw(n,h2_channel) * H2_PROD_AUX_OUT(n,h2_channel,h) * h2_hyd_liq_c_vom(n,h2_channel) )
* prod_site_storage
                + sum( (n,h2_channel) , h2_sto_p_sw(n,h2_channel) * h2_sto_p_c_a_overnight(n,h2_channel) * H2_N_STO(n,h2_channel) )
* aux_bftrans
                + sum ( (n,h2_channel) , h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_c_a_overnight(n,h2_channel) * H2_N_AUX_PRETRANS(n,h2_channel) )
* transportation
                + sum ( (n,h2_channel) , h2_trans_sw(n,h2_channel) * h2_trans_c_a_overnight(n,h2_channel) * H2_N_TRANS(n,h2_channel) )
                + sum ( (n,h2_channel,h) , h2_trans_sw(n,h2_channel) * h2_trans_c_var(n,h2_channel) * H2_AUX_PRETRANS_OUT(n,h2_channel,h) * h2_trans_dist(n,h2_channel))

* aux_bflp_storage
                + sum( (n,h2_channel) , h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_c_a_overnight(n,h2_channel) * H2_N_AUX_BFLP_STO(n,h2_channel) )

* lp_storage
                + sum ( (n,h2_channel) , h2_lp_sto_sw(n,h2_channel) * h2_lp_sto_c_a_overnight(n,h2_channel) * H2_N_LP_STO(n,h2_channel) )
* dehydration_evaporation
                + sum ( (n,h2_channel) , h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_c_a_overnight(n,h2_channel) * H2_N_DEHYD_EVAP(n,h2_channel) )
                + sum ( (n,h2_channel,h) , h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_gas_sw(n,h2_channel) * h2_dehyd_evap_gas(n,h2_channel) * h2_c_gas(n) * H2_LP_STO_OUT(n,h2_channel,h) )
                + sum ( (n,h2_channel,h) , h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_gas_sw(n,h2_channel) * h2_dehyd_evap_gas(n,h2_channel) * H2_LP_STO_OUT(n,h2_channel,h) * 0.001 * carbon_content(n,'CCGT') * CO2price(n,'CCGT') )

* aux_bfMP_storage
                + sum( (n,h2_channel) , h2_aux_bfMP_sto_sw(n,h2_channel) * h2_aux_bfMP_sto_c_a_overnight(n,h2_channel) * H2_N_AUX_BFMP_STO(n,h2_channel) )


* MP_storage
                + sum( (n,h2_channel) , h2_MP_sto_sw(n,h2_channel) * h2_MP_sto_c_a_overnight(n,h2_channel) * H2_N_MP_STO(n,h2_channel) )

* aux_bffilling_storage
                + sum ( (n,h2_channel) ,  h2_aux_bfhp_sto_sw(n,h2_channel) * aux_bfhp_sto_c_a_overnight(n,h2_channel) * H2_N_AUX_BFHP_STO(n,h2_channel) )
* filling_storage
                + sum ( (n,h2_channel) , h2_hp_sto_sw(n,h2_channel) * h2_hp_sto_c_a_overnight(n,h2_channel) * H2_N_HP_STO(n,h2_channel) )

* aux_bffuel_storage
                + sum( (n,h2_channel) , h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_c_a_overnight(n,h2_channel) * H2_N_AUX_BFFUEL(n,h2_channel) )

* aux_recon_site
                + sum ( (n,h2_channel,h2_tech_recon) , h2_recon_aux_sw(n,h2_channel,h2_tech_recon) * h2_recon_aux_c_a_overnight(n,h2_channel,h2_tech_recon) * H2_N_RECON_AUX(n,h2_channel,h2_tech_recon) )
* recon
                + sum ( (n,h2_tech_recon) ,  h2_recon_c_a_overnight(n,h2_tech_recon) * H2_N_RECON_extensive(n,h2_tech_recon) )
                + sum ( (n,h2_channel,h2_tech_recon,h) , H2_RECON_AUX_OUT(n,h2_channel,h2_tech_recon,h) * h2_recon_c_vom(n,h2_tech_recon) )

$ontext
$offtext

;

* ---------------------------------------------------------------------------- *
***** Energy balance and load levels *****
* ---------------------------------------------------------------------------- *

* Energy balance
con1a_bal(n,hh)..
         ( 1 - phi_pro_load(n) ) * d(n,hh) + sum( map_n_sto(n,sto) , STO_IN(n,sto,hh) )
%DSM%$ontext
        + sum( map_n_dsm(n,dsm_shift) , DSM_UP_DEMAND(n,dsm_shift,hh) )
$ontext
$offtext
%EV%$ontext
        + sum( map_n_ev(n,ev) , EV_CHARGE(n,ev,hh) )
$ontext
$offtext
%prosumage%$ontext
        + G_MARKET_M2PRO(n,hh)
        + sum( map_n_sto_pro(n,sto) , STO_IN_M2PRO(n,sto,hh))
        + sum( map_n_sto_pro(n,sto) , STO_IN_M2M(n,sto,hh))
$ontext
$offtext
%heat%$ontext
        + sum( map_n_heat(n,heat_building,heat_sink,heat_tech) , HEAT_ELEC_IN(n,heat_building,heat_sink,heat_tech,hh) )
*        + sum( (bu,ch) , theta_dir(n,bu,ch)   * (H_DIR(n,bu,ch,hh)     + H_DHW_DIR(n,bu,ch,hh)) )
*        + sum( (bu,ch) , theta_sets(n,bu,ch)  * (H_SETS_IN(n,bu,ch,hh) + H_DHW_AUX_ELEC_IN(n,bu,ch,hh)) )
*        + sum( (bu,hp) , theta_hp(n,bu,hp)    * H_HP_IN(n,bu,hp,hh) )
*        + sum( (bu,hel), theta_elec(n,bu,hel) * H_ELECTRIC_IN(n,bu,hel,hh) )
$ontext
$offtext

%P2H2P_parsimonious%$ontext
        + sum( (map_n_h2_ely(n,h2_ely),map_n_h2_sto(n,h2_sto)), H2_ELY_IN(n,h2_ely,hh) + H2_STO_IN(n,h2_sto,hh) * h2_comp_ed_ratio(n,h2_ely))
$ontext
$offtext

%P2H2_extensive%$ontext
* 0.001 to convert kWh electricity demand into MWh demand.
      + 0.001 * (
* prod
                 sum( (h2_tech,h2_channel) , H2_E_H2_IN(n,h2_tech,h2_channel,hh) )
* aux_prod_site
                 + sum( (h2_tech,h2_channel) , h2_prod_aux_sw(n,h2_tech,h2_channel) * h2_prod_aux_ed(n,h2_tech,h2_channel) * H2_PROD_AUX_IN(n,h2_tech,h2_channel,hh) )
* hydration_liquefaction
                 + sum ( h2_channel , h2_hyd_liq_sw(n,h2_channel) * h2_hyd_liq_ed(n,h2_channel) * H2_PROD_AUX_OUT(n,h2_channel,hh) )
* prod_site_storage
                 + sum ( h2_channel , h2_sto_p_sw(n,h2_channel) * H2_STO_P_L(n,h2_channel,hh) * h2_sto_p_ed(n,h2_channel) )
* aux_bftrans
                 + sum ( h2_channel, h2_aux_pretrans_sw(n,h2_channel) * h2_aux_pretrans_ed(n,h2_channel) * (sum(h2_tech,h2_bypass_1_sw(n,h2_tech,h2_channel) * H2_BYPASS_1(n,h2_tech,h2_channel,hh)) + H2_STO_P_OUT(n,h2_channel,hh)) )
* aux_bflp_storage
                 + sum ( h2_channel, h2_aux_bflp_sto_sw(n,h2_channel) * h2_aux_bflp_sto_ed(n,h2_channel) *  H2_AUX_BFLP_STO_IN(n,h2_channel,hh) )
* lp_storage
                 + sum ( h2_channel , h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L(n,h2_channel,hh) * h2_lp_sto_ed(n,h2_channel) )
* dehydration_evaporation
                 + sum ( h2_channel , ( 1 - h2_dehyd_evap_gas_sw(n,h2_channel) ) * h2_dehyd_evap_sw(n,h2_channel) * h2_dehyd_evap_ed(n,h2_channel) * H2_LP_STO_OUT(n,h2_channel,hh) )
* aux_bfMP_storage
                 + sum ( h2_channel, h2_aux_bfMP_sto_sw(n,h2_channel) * h2_aux_bfMP_sto_ed(n,h2_channel) * H2_DEHYD_EVAP_OUT(n,h2_channel,hh) )
* MP_storage
                 + sum ( h2_channel , h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L(n,h2_channel,hh) * h2_Mp_sto_ed(n,h2_channel) )
* aux_bffilling_storage
                 + sum ( h2_channel , h2_aux_bfhp_sto_sw(n,h2_channel) * h2_aux_bfhp_sto_ed(n,h2_channel) * H2_AUX_BFHP_STO_IN(n,h2_channel,hh) )
* filling storage
                 + sum ( h2_channel , h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L(n,h2_channel,hh) * h2_hp_sto_ed(n,h2_channel) )
* aux_bffuel
                 + sum ( h2_channel , h2_aux_bffuel_sw(n,h2_channel) * h2_aux_bffuel_ed(n,h2_channel) * H2_HP_STO_OUT(n,h2_channel,hh) )


* aux_recon_site
                 + sum ( (h2_channel,h2_tech_recon) , h2_recon_aux_sw(n,h2_channel,h2_tech_recon) * h2_recon_aux_ed(n,h2_channel,h2_tech_recon) * H2_RECON_AUX_OUT(n,h2_channel,h2_tech_recon,hh)/(1-H2_eta_recon_aux(n,h2_channel,h2_tech_recon)) )

                 )

$ontext
$offtext

        =E=

          sum( map_n_tech(n,dis) ,    G_L(n,dis,hh)) 
        + sum( map_n_tech(n,nondis) , G_RES(n,nondis,hh)) 
        + sum( map_n_sto(n,sto) ,     STO_OUT(n,sto,hh) ) 
        - sum( map_l(l) , inc(n,l) * eta_ntc(l) * F(l,hh))

%DSM%$ontext
        + sum( map_n_dsm(n,dsm_curt)  , DSM_CU(n,dsm_curt,hh))
        + sum( map_n_dsm(n,dsm_shift) , DSM_DO_DEMAND(n,dsm_shift,hh))
$ontext
$offtext
%EV%$ontext
        + sum( map_n_ev(n,ev) , EV_DISCHARGE(n,ev,hh) )
$ontext
$offtext
%prosumage%$ontext
        + sum( map_n_res_pro(n,res) , G_MARKET_PRO2M(n,res,hh) )
        + sum( map_n_sto_pro(n,sto) , STO_OUT_PRO2M(n,sto,hh))
        + sum( map_n_sto_pro(n,sto) , STO_OUT_M2M(n,sto,hh))
$ontext
$offtext

%P2H2P_parsimonious%$ontext
        + sum( (map_n_h2_recon(n,h2_recon)), H2_RECON_OUT(n,h2_recon,hh))
$ontext
$offtext

%P2H2_extensive%$ontext
* 0.001 to convert kWh electricity production into MWh demand.
      + 0.001 * (
* recon
         sum ( (h2_channel,h2_tech_recon) , h2_recon_sw(n,h2_channel) * H2_E_RECON_OUT(n,h2_channel,h2_tech_recon,hh) )

                )

$ontext
$offtext

        + G_INFES(n,hh)
;

con2a_loadlevel(n,dis,h)$(ord(h) > 1 AND map_n_tech(n,dis))..
        G_L(n,dis,h) =E= G_L(n,dis,h-1) + G_UP(n,dis,h) - G_DO(n,dis,h)
;

con2b_loadlevelstart(n,dis,h)$(ord(h) = 1 AND map_n_tech(n,dis))..
         G_L(n,dis,h) =E= G_UP(n,dis,h)
;

* ---------------------------------------------------------------------------- *
***** Hourly maximum generation caps   *****
* ---------------------------------------------------------------------------- *

con3a_maxprod_dispatchable(n,dis,h)$(map_n_tech(n,dis))..
        G_L(n,dis,h)  =L= avail(n,dis) * N_TECH(n,dis)
;

con3b_maxprod_res(n,nondis,h)$(map_n_tech(n,nondis))..
        G_RES(n,nondis,h) + CU(n,nondis,h)
            =E= phi_res(n,nondis,h) * avail(n,nondis) * N_TECH(n,nondis)
;


* ---------------------------------------------------------------------------- *
***** Storage constraints *****
* ---------------------------------------------------------------------------- *

***** Level constraints ******************************************************

*** Law of motion

* Storage level - general
con4a1_stolev(n,sto,h)$(map_n_sto(n,sto) AND (ord(h)>1))..
         STO_L(n,sto,h) =E=   STO_L(n,sto,h-1) * eta_sto_self(n,sto)
                            + STO_IN(n,sto,h)  * eta_sto_in(n,sto)
                            - STO_OUT(n,sto,h) / eta_sto_out(n,sto)
                            + sto_flow_in(n,sto,h)
;

* First hour
con4a2_stolev_ini(n,sto,h)$(map_n_sto(n,sto) AND ord(h) = 1)..
        STO_L(n,sto,h) =E=   STO_L_INI_LAST(n,sto)   
                           + STO_IN(n,sto,h) *  eta_sto_in(n,sto) 
                           - STO_OUT(n,sto,h) / eta_sto_out(n,sto)
                           + sto_flow_in(n,sto,h)
                           
;

* Last hour
con4a3_stolev_last(n,sto,h)$(map_n_sto(n,sto) AND ord(h) = card(h))..
         STO_L(n,sto,h) =E= STO_L_INI_LAST(n,sto)
;

*** Min/max level constraints

* Maximum storage level
con4b_stolev_max(n,sto,h)$(map_n_sto(n,sto))..
        STO_L(n,sto,h) =L= avail_sto(n,sto) * N_STO_E(n,sto)
;

* Exogenous minimum storage level
con4c_stolev_min(n,sto,h)$(map_n_sto(n,sto))..
         STO_L(n,sto,h) =G= phi_sto_lev_min(n,sto) * avail_sto(n,sto) * N_STO_E(n,sto)
;

***** Storage in/out constraints *********************************************

* Maximum storage in
con4d_maxin_sto(n,sto,h)$(map_n_sto(n,sto))..
        STO_IN(n,sto,h) =L= avail_sto(n,sto) * N_STO_P_IN(n,sto)
;

* Maximum storage out
con4e_maxout_sto(n,sto,h)$(map_n_sto(n,sto))..
        STO_OUT(n,sto,h) =L= avail_sto(n,sto) * N_STO_P_OUT(n,sto)
;

* Maximum storage in, such that storage doesn't turn full
con4f_maxin_lev(n,sto,h)$(map_n_sto(n,sto))..
        STO_IN(n,sto,h) * eta_sto_in(n,sto) =L=      avail_sto(n,sto) * N_STO_E(n,sto) 
                                                - eta_sto_self(n,sto) * STO_L(n,sto,h-1)
;

* Maximum storage out, such that storage doesn't turn empty (less than level t-1)
con4g_maxout_lev(n,sto,h)$(map_n_sto(n,sto))..
        STO_OUT(n,sto,h) / eta_sto_out(n,sto) =L= eta_sto_self(n,sto) * STO_L(n,sto,h-1)
;

***** Other ******************************************************************

* Storage level E-to-P ratio
con4h_EtoP(n,sto)$(map_n_sto(n,sto))..
        N_STO_E(n,sto) =L= etop_max(n,sto) * N_STO_P_OUT(n,sto)
;

* ---------------------------------------------------------------------------- *
***** Quotas for renewables and biomass *****
* ---------------------------------------------------------------------------- *

* RES share constraint: G_RES-and demand-based, storage losses completely covered by RES (original implementation)

con5a_minRES(n)$(phi_min_res(n)>0)..
        sum( h , G_L(n,'bio',h) 
                + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h))
                + sum( map_n_sto(n,sto),  sto_flow_in(n,sto,h))
*                 + sum( map_n_rsvr(n,rsvr) , RSVR_OUT(n,rsvr,h))

%prosumage%$ontext
        + sum( map_n_sto_pro(n,sto) , STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h)) 
        + sum( map_n_res_pro(n,res) , G_MARKET_PRO2M(n,res,h) + G_RES_PRO(n,res,h))
$ontext
$offtext

        )

        =G= 

        phi_min_res(n) * sum( h , d(n,h))

        + sum( (sto,h), STO_IN(n,sto,h) + sto_flow_in(n,sto,h) - STO_OUT(n,sto,h) )

%heat%$ontext
        
        + sum( (heat_building,heat_sink,heat_tech,h), HEAT_ELEC_IN(n,heat_building,heat_sink,heat_tech,h)$(heat_share(n,heat_building,heat_sink,heat_tech) > 0))

*        + sum( (bu,hp,h) , theta_hp(n,bu,hp) * H_HP_IN(n,bu,hp,h) )
*        + sum( (bu,ch,h),                          H_STO_IN_HP(n,bu,ch,h)
*                        + theta_elec(n,bu,ch)    * H_STO_IN_ELECTRIC(n,bu,ch,h)
*                        + theta_fossil(n,bu,ch)  * H_STO_IN_FOSSIL(n,bu,ch,h)
*                        - theta_storage(n,bu,ch) * H_STO_OUT(n,bu,ch,h)
*                        - theta_storage(n,bu,ch) * H_DHW_STO_OUT(n,bu,ch,h)
*             )
*
*        + phi_min_res(n) * sum( (bu,hp,h) , H_HP_IN(n,bu,hp,h) )
* oder:     +sum( (n, bu, hst, h), eta_heat_stat(n,bu,hst)*H_STO_LEV(n,bu,hst,h))
$ontext
$offtext

%EV%$ontext
%EV_EXOG%   + sum( (ev,h), EV_CHARGE(n,ev,h) - EV_GED(n,ev,h)/eta_ev_in(n,ev) - EV_DISCHARGE(n,ev,h) - (EV_L(n,ev,h)$(ord(h)=card(h)) - EV_L(n,ev,h)$(ord(h)=1)) )
$ontext
$offtext

%P2H2P_parsimonious%$ontext
            + sum( (map_n_h2_ely(n,h2_ely),map_n_h2_sto(n,h2_sto),map_n_h2_recon(n,h2_recon),h), H2_ELY_IN(n,h2_ely,h) + H2_STO_IN(n,h2_sto,h) * h2_comp_ed_ratio(n,h2_ely) - H2_RECON_OUT(n,h2_recon,h) )
$ontext
$offtext
;

con5b_max_energy(n,dis)$(map_n_tech(n,dis) AND max_e(n,dis))..
        sum( h , G_L(n,dis,h) ) =L= max_e(n,dis)
;

* ---------------------------------------------------------------------------- *
***** DSM constraints - curtailment *****
* ---------------------------------------------------------------------------- *

con6a_DSMcurt_duration_max(n,dsm_curt,h)$(map_n_dsm(n,dsm_curt))..
        sum( hh$( ord(hh) >= ord(h) AND ord(hh) < ord(h) + t_off_dsm_cu(n,dsm_curt) ) , DSM_CU(n,dsm_curt,hh) )
        =L= 
        N_DSM_CU(n,dsm_curt) * t_dur_dsm_cu(n,dsm_curt)
;

con6b_DSMcurt_max(n,dsm_curt,h)$(map_n_dsm(n,dsm_curt))..
        DSM_CU(n,dsm_curt,h)
        =L= 
        N_DSM_CU(n,dsm_curt)
;

* ---------------------------------------------------------------------------- *
***** DSM constraints - shifting *****
* ---------------------------------------------------------------------------- *

con7a_DSMshift_upanddown(n,dsm_shift,h)$(map_n_dsm(n,dsm_shift))..
        DSM_UP(n,dsm_shift,h) * (1 + eta_dsm_shift(n,dsm_shift))/2 =E= 2/(1+eta_dsm_shift(n,dsm_shift)) * sum( hh$( ord(hh) >= ord(h) - t_dur_dsm_shift(n,dsm_shift) AND ord(hh) <= ord(h) + t_dur_dsm_shift(n,dsm_shift) ) , DSM_DO(n,dsm_shift,h,hh))
;

con7b_DSMshift_granular_max(n,dsm_shift,h)$(map_n_dsm(n,dsm_shift))..
        DSM_UP_DEMAND(n,dsm_shift,h) + DSM_DO_DEMAND(n,dsm_shift,h)
        =L= 
        N_DSM_SHIFT(n,dsm_shift)
;

con7c_DSM_distrib_up(n,dsm_shift,h)$(map_n_dsm(n,dsm_shift))..
        DSM_UP(n,dsm_shift,h) =E= DSM_UP_DEMAND(n,dsm_shift,h)
;

con7d_DSM_distrib_do(n,dsm_shift,h)$(map_n_dsm(n,dsm_shift))..
        sum( hh$( ord(hh) >= ord(h) - t_dur_dsm_shift(n,dsm_shift) AND ord(hh) <= ord(h) + t_dur_dsm_shift(n,dsm_shift) ) , DSM_DO(n,dsm_shift,hh,h) )
        =E=
        DSM_DO_DEMAND(n,dsm_shift,h)
;

con7e_DSMshift_recovery(n,dsm_shift,h)$(map_n_dsm(n,dsm_shift))..
        sum( hh$( ord(hh) >= ord(h) AND ord(hh) < ord(h) + t_off_dsm_shift(n,dsm_shift) ) , DSM_UP(n,dsm_shift,hh))
        =L= N_DSM_SHIFT(n,dsm_shift)
;

* ---------------------------------------------------------------------------- *
***** Minimum and maximum installation constraints *****
* ---------------------------------------------------------------------------- *

con8a1_min_I_power(n,tech)$(map_n_tech(n,tech))..
        N_TECH(n,tech) =G= min_p(n,tech)
;

con8a2_max_I_power(n,tech)$(map_n_tech(n,tech))..
        N_TECH(n,tech) =L= max_p(n,tech)
;

con8b1_min_I_sto_e(n,sto)$(map_n_sto(n,sto))..
        N_STO_E(n,sto) =G= min_sto_e(n,sto)
;

con8b2_max_I_sto_e(n,sto)$(map_n_sto(n,sto))..
        N_STO_E(n,sto) =L= max_sto_e(n,sto)
;

con8c1_min_I_sto_p_in(n,sto)$(map_n_sto(n,sto))..
        N_STO_P_IN(n,sto) =G= min_sto_p_in(n,sto)
;

con8c2_max_I_sto_p_in(n,sto)$(map_n_sto(n,sto))..
        N_STO_P_IN(n,sto) =L= max_sto_p_in(n,sto)
;

con8d1_min_I_sto_p_out(n,sto)$(map_n_sto(n,sto))..
        N_STO_P_OUT(n,sto) =G= min_sto_p_out(n,sto)
;

con8d2_max_I_sto_p_out(n,sto)$(map_n_sto(n,sto))..
        N_STO_P_OUT(n,sto) =L= max_sto_p_out(n,sto)
;

con8e_max_I_dsm_cu(n,dsm_curt)$(map_n_dsm(n,dsm_curt))..
        N_DSM_CU(n,dsm_curt) =L= m_dsm_cu(n,dsm_curt)
;

con8f_max_I_dsm_shift_pos(n,dsm_shift)$(map_n_dsm(n,dsm_shift))..
        N_DSM_SHIFT(n,dsm_shift) =L= m_dsm_shift(n,dsm_shift)
;

con8g_max_pro_res(n,res)$(map_n_res_pro(n,res))..
        N_RES_PRO(n,res) =L= m_res_pro(n,res)
;

con8h_max_pro_sto_e(n,sto)$(map_n_sto_pro(n,sto))..
        N_STO_E_PRO(n,sto) =L= m_sto_pro_e(n,sto)
;

con8i_max_sto_pro_p(n,sto)$(map_n_sto_pro(n,sto))..
        N_STO_P_PRO(n,sto) =L= m_sto_pro_p(n,sto)
;

con8j1a_min_I_ntc_ex(l)$(map_l(l))..
        NTC_ex(l) =G= min_ntc_ex(l)
;

con8j1b_max_I_ntc_ex(l)$(map_l(l))..
        NTC_ex(l) =L= max_ntc_ex(l)
;

con8j2a_min_I_ntc_im(l)$(map_l(l))..
        NTC_im(l) =G= min_ntc_im(l)
;

con8j2b_max_I_ntc_im(l)$(map_l(l))..
        NTC_im(l) =L= max_ntc_im(l)
;

con8k1_min_I_h2_ely_p(n,h2_ely)$(map_n_h2_ely(n,h2_ely))..
        H2_N_ELY(n,h2_ely) =G= min_h2_ely_p(n,h2_ely)
;

con8k2_max_I_h2_ely_p(n,h2_ely)$(map_n_h2_ely(n,h2_ely))..
        H2_N_ELY(n,h2_ely) =L= max_h2_ely_p(n,h2_ely)
;

con8l1_min_I_h2_sto_e(n,h2_sto)$map_n_h2_sto(n,h2_sto)..
        H2_N_STO_E(n,h2_sto) =G= min_h2_sto_e(n,h2_sto)
;

con8l2_max_I_h2_sto_e(n,h2_sto)$map_n_h2_sto(n,h2_sto)..
        H2_N_STO_E(n,h2_sto) =L= max_h2_sto_e(n,h2_sto)
;

con8m1_min_I_h2_recon_p(n,h2_recon)$(map_n_h2_recon(n,h2_recon))..
        H2_N_RECON(n,h2_recon) =G= min_h2_recon_p(n,h2_recon)
;

con8m2_max_I_h2_recon_p(n,h2_recon)$(map_n_h2_recon(n,h2_recon))..
        H2_N_RECON(n,h2_recon) =L= max_h2_recon_p(n,h2_recon)
;


* ---------------------------------------------------------------------------- *
***** parsimonious P2H2P module constraints *****
* ---------------------------------------------------------------------------- *

* h2 electrolysis
* TODO: test mapping vs. feat_node: maybe both (map_n_h2_ely/map_n_tech_h2(h2_ely/tech_h2,n))
con9a_h2_ely_conv(n,h2_ely,h2_sto,h)$feat_node('hydrogen_parsimonious',n)..
        H2_ELY_IN(n,h2_ely,h) * eta_h2_ely(n,h2_ely) * eta_h2_comp(n,h2_ely) =E= H2_STO_IN(n,h2_sto,h)
;

*map_n_h2_ely/map_n_tech_h2(h2_ely/tech_h2,n)
con9b_h2_ely_conv_max(n,h2_ely,h)$feat_node('hydrogen_parsimonious',n)..
        H2_ELY_IN(n,h2_ely,h) =L= avail_h2_ely(n,h2_ely) * H2_N_ELY(n,h2_ely)
;

* H2 storage
*map_n_h2_sto(h2_sto,n)
con9c_h2_sto_lev_start(n,h2_sto,h)$(feat_node('hydrogen_parsimonious',n) AND (ord(h)=1))..
        H2_STO_L(n,h2_sto,h) 
        =E= 
        H2_STO_L_INI_LAST(n,h2_sto) + H2_STO_IN(n,h2_sto,h) - H2_STO_OUT(n,h2_sto,h) - d_h2_parsimonious(n,h)
;

con9d_h2_sto_lev(n,h2_sto,h)$(feat_node('hydrogen_parsimonious',n) AND (ord(h)>1))..
        H2_STO_L(n,h2_sto,h) 
        =E= 
        eta_h2_sto_self(n,h2_sto) * H2_STO_L(n,h2_sto,h-1) + H2_STO_IN(n,h2_sto,h) - H2_STO_OUT(n,h2_sto,h) - d_h2_parsimonious(n,h)
;

con9e_h2_sto_lev_end(n,h2_sto,h)$(feat_node('hydrogen_parsimonious',n) AND (ord(h)=card(h)))..
        H2_STO_L(n,h2_sto,h) 
        =E= 
        H2_STO_L_INI_LAST(n,h2_sto)
;

con9f_h2_sto_lev_max(n,h2_sto,h)$feat_node('hydrogen_parsimonious',n)..
        H2_STO_L(n,h2_sto,h)
        =L=
        avail_h2_sto(n,h2_sto) * H2_N_STO_E(n,h2_sto)
;

con9g_h2_sto_lev_min(n,h2_sto,h)$feat_node('hydrogen_parsimonious',n)..
        H2_STO_L(n,h2_sto,h)
        =G=
        phi_h2_sto_min(n,h2_sto) * avail_h2_sto(n,h2_sto) * H2_N_STO_E(n,h2_sto)
;

* H2 reconversion
con9h_h2_recon(n,h2_recon,h2_sto,h)$feat_node('hydrogen_parsimonious',n)..
        H2_STO_OUT(n,h2_sto,h) * eta_h2_recon(n,h2_recon) =E= H2_RECON_OUT(n,h2_recon,h)
;

con9i_h2_recon_max(n,h2_recon,h)$feat_node('hydrogen_parsimonious',n)..
        H2_RECON_OUT(n,h2_recon,h) =L= avail_h2_recon(n,h2_recon) * H2_N_RECON(n,h2_recon)
;



* ---------------------------------------------------------------------------- *
***** Electric vehicle constraints *****
* ---------------------------------------------------------------------------- *

con10a_ev_ed(n,ev,h)$(map_n_ev(n,ev))..
        feat_node('ev',n) *
        ev_ed(n,ev,h) * phi_ev(n,ev) * ev_quant(n)
        =E= EV_GED(n,ev,h) + EV_PHEVFUEL(n,ev,h)$(ev_phev(n,ev)=1)
;

con10b_ev_chargelev_start(ev,h,n)$(map_n_ev(n,ev) AND ord(h) = 1 AND feat_node('ev',n))..
        EV_L(n,ev,h) =E= feat_node('ev',n) * phi_ev_ini(n,ev) * n_ev_e(n,ev) * phi_ev(n,ev) * ev_quant(n)
        + EV_CHARGE(n,ev,h) * eta_ev_in(n,ev)
        - EV_DISCHARGE(n,ev,h) / eta_ev_out(n,ev)
        - EV_GED(n,ev,h)
;

con10c_ev_chargelev(ev,h,n)$(map_n_ev(n,ev) AND ord(h) > 1 AND feat_node('ev',n))..
        EV_L(n,ev,h) =E= EV_L(n,ev,h-1)
        + EV_CHARGE(n,ev,h) * eta_ev_in(n,ev)
        - EV_DISCHARGE(n,ev,h) / eta_ev_out(n,ev)
        - EV_GED(n,ev,h)
;

* con10c_ev_chargelev(ev,h,n)$(map_n_ev(n,ev) AND feat_node('ev',n))..
*          EV_L(n,ev,h) =E= EV_L(n,ev,h--1)
*          + EV_CHARGE(n,ev,h) * eta_ev_in(n,ev)
*          - EV_DISCHARGE(n,ev,h) / eta_ev_out(n,ev)
*          - EV_GED(n,ev,h)
* ;

con10d_ev_chargelev_max(n,ev,h)$(map_n_ev(n,ev) AND feat_node('ev',n))..
        EV_L(n,ev,h)
         =L= n_ev_e(n,ev) * phi_ev(n,ev) * ev_quant(n)
             * feat_node('ev',n)
;

con10e_ev_maxin(n,ev,h)$(map_n_ev(n,ev) AND feat_node('ev',n))..
        EV_CHARGE(n,ev,h)
        =L= 
        n_ev_p(n,ev,h) * phi_ev(n,ev) * ev_quant(n) * feat_node('ev',n)
;

con10f_ev_maxout(n,ev,h)$(map_n_ev(n,ev) AND feat_node('ev',n))..
        EV_DISCHARGE(n,ev,h)
        =L= 
        n_ev_p(n,ev,h) * phi_ev(n,ev) * ev_quant(n) * feat_node('ev',n)
;

con10g_ev_chargelev_ending(n,ev,h)$(map_n_ev(n,ev) AND ord(h) = card(h) AND feat_node('ev',n))..
         EV_L(n,ev,h) =G= phi_ev_ini(n,ev) * n_ev_e(n,ev) * phi_ev(n,ev) * ev_quant(n) * feat_node('ev',n)
;

con10h_ev_maxin_lev(n,ev,h)$(map_n_ev(n,ev))..
        ( EV_CHARGE(n,ev,h)
        ) * eta_ev_in(n,ev)
        =L= n_ev_e(n,ev) * phi_ev(n,ev) * ev_quant(n) - EV_L(n,ev,h-1)
;

con10i_ev_maxout_lev(n,ev,h)$(map_n_ev(n,ev) AND feat_node('ev',n))..
        EV_DISCHARGE(n,ev,h) / eta_ev_out(n,ev)
        =L= EV_L(n,ev,h-1)
;

con10k_ev_exog(n,ev,h)$(map_n_ev(n,ev) AND feat_node('ev',n))..
        EV_CHARGE(n,ev,h)
        =E=
        ev_ged_exog(n,ev,h) * phi_ev(n,ev) * ev_quant(n)
        * feat_node('ev',n)
;

* ---------------------------------------------------------------------------- *
***** Prosumage constraints *****
* ---------------------------------------------------------------------------- *

con11a_pro_distrib(n,res,h)$(map_n_res_pro(n,res))..
        phi_res(n,res,h) * avail(n,res) * N_RES_PRO(n,res)
        =E=
        CU_PRO(n,res,h) + G_MARKET_PRO2M(n,res,h) + G_RES_PRO(n,res,h) + sum( map_n_sto_pro(n,sto) , STO_IN_PRO2PRO(n,res,sto,h) + STO_IN_PRO2M(n,res,sto,h) )
;

con11b_pro_balance(n,h)..
        phi_pro_load(n) * d(n,h)
        =E=
        sum( map_n_res_pro(n,res) , G_RES_PRO(n,res,h)) + sum( map_n_sto_pro(n,sto) , STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_M2PRO(n,sto,h) ) + G_MARKET_M2PRO(n,h)
;

con11c_pro_selfcon(n)..
        sum( (map_n_res_pro(n,res),h) , G_RES_PRO(n,res,h) ) + sum( (sto,h) , STO_OUT_PRO2PRO(n,sto,h) )
        =G=
         phi_pro_self(n) * sum( h , phi_pro_load(n) * d(n,h))
;

con11d_pro_stolev_PRO2PRO(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) > 1 )..
        STO_L_PRO2PRO(n,sto,h) =E= STO_L_PRO2PRO(n,sto,h-1) + sum( map_n_res_pro(n,res) , STO_IN_PRO2PRO(n,res,sto,h)*eta_sto_in(n,sto) - STO_OUT_PRO2PRO(n,sto,h)/eta_sto_out(n,sto) )
;

con11e_pro_stolev_PRO2M(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) > 1)..
        STO_L_PRO2M(n,sto,h) =E= STO_L_PRO2M(n,sto,h-1) + sum( map_n_res_pro(n,res) , STO_IN_PRO2M(n,res,sto,h)*eta_sto_in(n,sto) - STO_OUT_PRO2M(n,sto,h)/eta_sto_out(n,sto) )
;

con11f_pro_stolev_M2PRO(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) > 1)..
        STO_L_M2PRO(n,sto,h) =E= STO_L_M2PRO(n,sto,h-1) + STO_IN_M2PRO(n,sto,h)*eta_sto_in(n,sto) - STO_OUT_M2PRO(n,sto,h)/eta_sto_out(n,sto)
;

con11g_pro_stolev_M2M(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) > 1)..
        STO_L_M2M(n,sto,h) =E= STO_L_M2M(n,sto,h-1) + STO_IN_M2M(n,sto,h)*eta_sto_in(n,sto) - STO_OUT_M2M(n,sto,h)/eta_sto_out(n,sto)
;

* con11d_pro_stolev_PRO2PRO(n,sto,h)$(map_n_sto_pro(n,sto))..
*          STO_L_PRO2PRO(n,sto,h) =E= STO_L_PRO2PRO(n,sto,h--1) + sum( map_n_res_pro(n,res) , STO_IN_PRO2PRO(n,res,sto,h)*eta_sto_in(n,sto) - STO_OUT_PRO2PRO(n,sto,h)/eta_sto_out(n,sto) )
* ;
* 
* con11e_pro_stolev_PRO2M(n,sto,h)$(map_n_sto_pro(n,sto))..
*          STO_L_PRO2M(n,sto,h) =E= STO_L_PRO2M(n,sto,h--1) + sum( map_n_res_pro(n,res) , STO_IN_PRO2M(n,res,sto,h)*eta_sto_in(n,sto) - STO_OUT_PRO2M(n,sto,h)/eta_sto_out(n,sto) )
* ;
* 
* con11f_pro_stolev_M2PRO(n,sto,h)$(map_n_sto_pro(n,sto))..
*          STO_L_M2PRO(n,sto,h) =E= STO_L_M2PRO(n,sto,h--1) + STO_IN_M2PRO(n,sto,h)*eta_sto_in(n,sto) - STO_OUT_M2PRO(n,sto,h)/eta_sto_out(n,sto)
* ;
* 
* con11g_pro_stolev_M2M(n,sto,h)$(map_n_sto_pro(n,sto))..
*          STO_L_M2M(n,sto,h) =E= STO_L_M2M(n,sto,h--1) + STO_IN_M2M(n,sto,h)*eta_sto_in(n,sto) - STO_OUT_M2M(n,sto,h)/eta_sto_out(n,sto)
* ;

*con11h_1_pro_stolev_start_PRO2PRO(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) = 1)..
*        STO_L_PRO2PRO(n,sto,h) =E= 0.25 * phi_sto_pro_ini(n,sto) * N_STO_E_PRO(n,sto) + sum( map_n_res_pro(n,res) , STO_IN_PRO2PRO(n,res,sto,h))*eta_sto_in(n,sto) - STO_OUT_PRO2PRO(n,sto,h)/eta_sto_out(n,sto)
*;

*con11h_2_pro_stolev_start_PRO2M(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) = 1)..
*        STO_L_PRO2M(n,sto,h) =E= 0.25 * phi_sto_pro_ini(n,sto) * N_STO_E_PRO(n,sto) + sum( map_n_res_pro(n,res) , STO_IN_PRO2M(n,res,sto,h))*eta_sto_in(n,sto) - STO_OUT_PRO2M(n,sto,h)/eta_sto_out(n,sto)
*;

*con11h_3_pro_stolev_start_M2PRO(n,sto,h)$(map_n_sto_pro(n,sto) AND map_n_sto_pro(n,sto) AND ord(h) = 1)..
*        STO_L_M2PRO(n,sto,h) =E= 0.25 * phi_sto_pro_ini(n,sto) * N_STO_E_PRO(n,sto) + STO_IN_M2PRO(n,sto,h)*eta_sto_in(n,sto) - STO_OUT_M2PRO(n,sto,h)/eta_sto_out(n,sto)
*;

*con11h_4_pro_stolev_start_M2M(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) = 1)..
*        STO_L_M2M(n,sto,h) =E= 0.25 * phi_sto_pro_ini(n,sto) * N_STO_E_PRO(n,sto) + STO_IN_M2M(n,sto,h)*eta_sto_in(n,sto) - STO_OUT_M2M(n,sto,h)/eta_sto_out(n,sto)
*;

con11i_pro_stolev(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h)>1)..
        STO_L_PRO(n,sto,h) =E= STO_L_PRO2PRO(n,sto,h) +  STO_L_PRO2M(n,sto,h) + STO_L_M2PRO(n,sto,h) + STO_L_M2M(n,sto,h)
;

* con11i_pro_stolev(n,sto,h)$(map_n_sto_pro(n,sto))..
*          STO_L_PRO(n,sto,h) =E= STO_L_PRO2PRO(n,sto,h) +  STO_L_PRO2M(n,sto,h) + STO_L_M2PRO(n,sto,h) + STO_L_M2M(n,sto,h)
* ;

con11j_pro_stolev_max(n,sto,h)$map_n_sto_pro(n,sto)..
        STO_L_PRO(n,sto,h) =L= avail_sto(n,sto) * N_STO_E_PRO(n,sto)
;

con11k_pro_maxin_sto(n,sto,h)$(map_n_sto_pro(n,sto))..
        sum( map_n_res_pro(n,res) , STO_IN_PRO2PRO(n,res,sto,h) + STO_IN_PRO2M(n,res,sto,h) ) + STO_IN_M2PRO(n,sto,h) + STO_IN_M2M(n,sto,h)
        =L= avail_sto(n,sto) * N_STO_P_PRO(n,sto)
;

con11l_pro_maxout_sto(n,sto,h)$(map_n_sto_pro(n,sto))..
        STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h) + STO_OUT_M2PRO(n,sto,h) + STO_OUT_M2M(n,sto,h)
        =L= avail_sto(n,sto) * N_STO_P_PRO(n,sto)
;

con11m_pro_maxout_lev(n,sto,h)$(map_n_sto_pro(n,sto))..
        ( STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_M2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h) + STO_OUT_M2M(n,sto,h) ) / eta_sto_out(n,sto)
        =L= STO_L_PRO(n,sto,h-1)
;

con11n_pro_maxin_lev(n,sto,h)$(map_n_sto_pro(n,sto))..
        ( sum( map_n_res_pro(n,res) , STO_IN_PRO2PRO(n,res,sto,h) + STO_IN_PRO2M(n,res,sto,h) ) + STO_IN_M2PRO(n,sto,h) + STO_IN_M2M(n,sto,h) ) * eta_sto_in(n,sto)
        =L= avail_sto(n,sto) * N_STO_E_PRO(n,sto) - STO_L_PRO(n,sto,h-1)
;

con11o_pro_ending(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) = card(h))..
        STO_L_PRO(n,sto,h) =E= phi_sto_pro_ini(n,sto) * avail_sto(n,sto) * N_STO_E_PRO(n,sto)
;

* con11m_pro_maxout_lev(n,sto,h)$(map_n_sto_pro(n,sto))..
*         ( STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_M2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h) + STO_OUT_M2M(n,sto,h) ) / eta_sto_out(n,sto)
*         =L= STO_L_PRO(n,sto,h--1)
* ;
* 
* con11n_pro_maxin_lev(n,sto,h)$(map_n_sto_pro(n,sto))..
*         ( sum( map_n_res_pro(n,res) , STO_IN_PRO2PRO(n,res,sto,h) + STO_IN_PRO2M(n,res,sto,h) ) + STO_IN_M2PRO(n,sto,h) + STO_IN_M2M(n,sto,h) ) * eta_sto_in(n,sto)
*         =L= avail_sto(n,sto) * N_STO_E_PRO(n,sto) - STO_L_PRO(n,sto,h--1)
* ;
* 
* *con11o_pro_ending(n,sto,h)$(map_n_sto_pro(n,sto) AND ord(h) = card(h))..
* *         STO_L_PRO(n,sto,h) =E= phi_sto_pro_ini(n,sto) * avail_sto(n,sto) * N_STO_E_PRO(n,sto)
* *;

* ---------------------------------------------------------------------------- *
***** NTC constraints *****
* ---------------------------------------------------------------------------- *

***** Constraint on energy flow between nodes ******
con12a_max_f(l,h)$(map_l(l))..
        F(l,h) =L= NTC_ex(l)
;

con12b_min_f(l,h)$(map_l(l))..
        F(l,h) =G= -NTC_im(l)
;


* ---------------------------------------------------------------------------- *
***** Heating constraints *****
* ---------------------------------------------------------------------------- *

* Energy balances

* Heat space balance
* The condition "$(heat_share(n,heat_building,heat_tech,'space') > 0)" ensure that the equation is only
* used/compiled for country-building-tech-sink-combinations that are actually defined, and not for 
* all combintions.

con_heat_bal(n,heat_building,heat_sink,heat_tech,h)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_STO_OUT(n,heat_building,heat_sink,heat_tech,h) 

        =E=

        heat_share(n,heat_building,heat_sink,heat_tech) * heat_demand(n,heat_building,heat_sink,h)

;

* Electricity demand
con_heat_elec_in(n,heat_building,heat_sink,heat_tech,h)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_STO_IN(n,heat_building,heat_sink,heat_tech,h)

        =E=

        HEAT_ELEC_IN(n,heat_building,heat_sink,heat_tech,h) * heat_cop(n,heat_sink,heat_tech,h)

;

* Storage law of motion
con_heat_sto_lev(n,heat_building,heat_sink,heat_tech,h)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech)  AND ( ord(h) > 1 ) )..

        HEAT_STO_LEV(n,heat_building,heat_sink,heat_tech,h)
        
        =E=

        (1 - heat_standing_losses(n,heat_building,heat_sink,heat_tech)) * 
        HEAT_STO_LEV(n,heat_building,heat_sink,heat_tech,h-1) + 
        HEAT_STO_IN(n,heat_building,heat_sink,heat_tech,h)  -
        HEAT_STO_OUT(n,heat_building,heat_sink,heat_tech,h)

;

* First hour
con_heat_sto_lev_ini(n,heat_building,heat_sink,heat_tech,h)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) AND ( ord(h) = 1 ) )..

        HEAT_STO_LEV(n,heat_building,heat_sink,heat_tech,h)

        =E=

        HEAT_STO_LEV_INI(n,heat_building,heat_sink,heat_tech) + 
        HEAT_STO_IN(n,heat_building,heat_sink,heat_tech,h)  -
        HEAT_STO_OUT(n,heat_building,heat_sink,heat_tech,h)
;

* Last hour
con_heat_sto_lev_end(n,heat_building,heat_sink,heat_tech,h)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) AND ( ord(h) = card(h) ) )..

        HEAT_STO_LEV(n,heat_building,heat_sink,heat_tech,h)

        =E=

        HEAT_STO_LEV_INI(n,heat_building,heat_sink,heat_tech)

;

* Installed heating power (out)
con_heat_n_sto_out(n,heat_building,heat_sink,heat_tech)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_N_STO_OUT(n,heat_building,heat_sink,heat_tech)

        =E= 

        heat_share(n,heat_building,heat_sink,heat_tech) * smax( h , heat_demand(n,heat_building,heat_sink,h) )

;

* Installed heating power (in)
con_heat_n_sto_in(n,heat_building,heat_sink,heat_tech)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_N_STO_OUT(n,heat_building,heat_sink,heat_tech)
        
        =E= 
        
        HEAT_N_STO_IN(n,heat_building,heat_sink,heat_tech)

;

* Installed heating energy
con_heat_n_sto_e(n,heat_building,heat_sink,heat_tech)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_N_STO_E(n,heat_building,heat_sink,heat_tech)
        
        =E= 
        
        heat_storage_ratio(n,heat_building,heat_sink,heat_tech) * HEAT_N_STO_OUT(n,heat_building,heat_sink,heat_tech)   

;

* Installed electric power 
con_heat_n_elec_in(n,heat_building,heat_sink,heat_tech)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_N_ELEC_IN(n,heat_building,heat_sink,heat_tech)

        =E=

        heat_downsize_fac(n,heat_building,heat_sink,heat_tech) * heat_share(n,heat_building,heat_sink,heat_tech) * smax( h ,  ( heat_demand(n,heat_building,heat_sink,h) / heat_cop(n,heat_sink,heat_tech,h) ) )

;

* Max storage level
con_heat_sto_lev_max(n,heat_building,heat_sink,heat_tech,h)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_STO_LEV(n,heat_building,heat_sink,heat_tech,h)

        =L=

        HEAT_N_STO_E(n,heat_building,heat_sink,heat_tech)

;

* Max electric input
con_heat_elec_in_max(n,heat_building,heat_sink,heat_tech,h)$( feat_node('heat',n) AND map_n_heat(n,heat_building,heat_sink,heat_tech) )..

        HEAT_ELEC_IN(n,heat_building,heat_sink,heat_tech,h)

        =L=

        HEAT_N_ELEC_IN(n,heat_building,heat_sink,heat_tech)

;


* theta_hp(n,bu,hst)*H_STO_IN_HP(n,bu,hst,h) + 
* theta_elec(n,bu,hst)*H_STO_IN_ELECTRIC(n,bu,hst,h) + 
* theta_fossil(n,bu,hst) * H_STO_IN_FOSSIL(n,bu,hst,h)
*          - H_STO_OUT(n,bu,hst,h) - H_DHW_STO_OUT(n,bu,hst,h)
* ;
* 
* *con14u_storage_level_start(n,bu,hst,h)$(feat_node('heat',n) AND theta_storage(n,bu,hst) AND (ord(h) = 1 OR ord(h) = card(h)))..
* *         H_STO_LEV(n,bu,hst,h) =E= phi_heat_ini(n,bu,hst) * theta_storage(n,bu,hst)*n_heat_e(n,bu,hst)

$ontext

con14a_heat_balance(n,bu,ch,h)$feat_node('heat',n)..

*con14a_heat_balance(n,bu,ch,h)$feat_node('heat',n)..
*        theta_dir(n,bu,ch) * H_DIR(n,bu,ch,h) + theta_sets(n,bu,ch) * H_SETS_OUT(n,bu,ch,h)+ theta_storage(n,bu,ch) * H_STO_OUT(n,bu,ch,h)
*        + theta_sets(n,bu,ch) * (1-eta_heat_stat(n,bu,ch)) * H_SETS_LEV(n,bu,ch,h-1)$(theta_sets(n,bu,ch) AND ord(h) > 1)
*        =G= dh(n,bu,ch,h) - H_INFES(n,bu,ch,h)
*;  

*con14b_dhw_balance(n,bu,ch,h)$feat_node('heat',n)..
*        theta_storage(n,bu,ch) * H_DHW_STO_OUT(n,bu,ch,h) + theta_dir(n,bu,ch) * H_DHW_DIR(n,bu,ch,h) + theta_sets(n,bu,ch) * H_DHW_AUX_OUT(n,bu,ch,h)
*        =E=
*        d_dhw(n,bu,ch,h) - H_DHW_INFES(n,bu,ch,h)
*;

* SETS
con14c_sets_level(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) AND ord(h) > 1)..
        H_SETS_LEV(n,bu,ch,h) =E= eta_heat_stat(n,bu,ch) * H_SETS_LEV(n,bu,ch,h-1) + eta_heat_dyn(n,bu,ch) * H_SETS_IN(n,bu,ch,h) - H_SETS_OUT(n,bu,ch,h)
;

con14d_sets_level_start(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) AND (ord(h) = 1 OR ord(h) = card(h)))..
        H_SETS_LEV(n,bu,ch,h) =E= phi_heat_ini(n,bu,ch) * n_sets_e(n,bu,ch)
;


* * Energy balances
* con14a_heat_balance(n,bu,ch,h)$feat_node('heat',n)..
*          theta_dir(n,bu,ch) * H_DIR(n,bu,ch,h) + theta_sets(n,bu,ch) * H_SETS_OUT(n,bu,ch,h)+ theta_storage(n,bu,ch) * H_STO_OUT(n,bu,ch,h)
*          + theta_sets(n,bu,ch) * (1-eta_heat_stat(n,bu,ch)) * H_SETS_LEV(n,bu,ch,h--1)$(theta_sets(n,bu,ch))
*          =G= dh(n,bu,ch,h) - H_INFES(n,bu,ch,h)
* ;
* 
* con14b_dhw_balance(n,bu,ch,h)$feat_node('heat',n)..
*          theta_storage(n,bu,ch) * H_DHW_STO_OUT(n,bu,ch,h) + theta_dir(n,bu,ch) * H_DHW_DIR(n,bu,ch,h) + theta_sets(n,bu,ch) * H_DHW_AUX_OUT(n,bu,ch,h)
*          =E=
*          d_dhw(n,bu,ch,h) - H_DHW_INFES(n,bu,ch,h)
* ;
* 
* * SETS
* con14c_sets_level(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch))..
*          H_SETS_LEV(n,bu,ch,h) =E= eta_heat_stat(n,bu,ch) * H_SETS_LEV(n,bu,ch,h--1) + eta_heat_dyn(n,bu,ch) * H_SETS_IN(n,bu,ch,h) - H_SETS_OUT(n,bu,ch,h)
* ;

*con14d_sets_level_start(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) AND (ord(h) = 1 OR ord(h) = card(h)))..
*         H_SETS_LEV(n,bu,ch,h) =E= phi_heat_ini(n,bu,ch) * n_sets_e(n,bu,ch)
*;

con14e_sets_maxin(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch))..
        H_SETS_IN(n,bu,ch,h)
        =L= 
        n_sets_p_in(n,bu,ch)
;

con14f_sets_maxout(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch))..
        H_SETS_OUT(n,bu,ch,h) =L= n_sets_p_out(n,bu,ch)
;

con14h_sets_maxlev(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch))..
        H_SETS_LEV(n,bu,ch,h) =L= n_sets_e(n,bu,ch)
;

* SETS and DHW
con14i_sets_aux_dhw_level(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) AND ord(h) > 1)..
        H_DHW_AUX_LEV(n,bu,ch,h) =E= eta_dhw_aux_stat(n,bu,ch) * H_DHW_AUX_LEV(n,bu,ch,h-1) + H_DHW_AUX_ELEC_IN(n,bu,ch,h) - H_DHW_AUX_OUT(n,bu,ch,h)
;

con14j_sets_aux_dhw_level_start(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) AND (ord(h) = 1 OR ord(h) = card(h)) )..
        H_DHW_AUX_LEV(n,bu,ch,h) =E= phi_heat_ini(n,bu,ch) * n_sets_dhw_e(n,bu,ch)
;

* con14i_sets_aux_dhw_level(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch))..
*          H_DHW_AUX_LEV(n,bu,ch,h) =E= eta_dhw_aux_stat(n,bu,ch) * H_DHW_AUX_LEV(n,bu,ch,h--1) + H_DHW_AUX_ELEC_IN(n,bu,ch,h) - H_DHW_AUX_OUT(n,bu,ch,h)
* ;

*con14j_sets_aux_dhw_level_start(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) AND (ord(h) = 1 OR ord(h) = card(h)) )..
*         H_DHW_AUX_LEV(n,bu,ch,h) =E= phi_heat_ini(n,bu,ch) * n_sets_dhw_e(n,bu,ch)
*;

con14k_sets_aux_dhw_maxin(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) )..
        H_DHW_AUX_ELEC_IN(n,bu,ch,h)
        =L= 
        n_sets_dhw_p_in(n,bu,ch)
;

con14m_sets_aux_dhw_maxlev(n,bu,ch,h)$(feat_node('heat',n) AND theta_sets(n,bu,ch) )..
        H_DHW_AUX_LEV(n,bu,ch,h) =L= n_sets_dhw_e(n,bu,ch)
;

* HEAT PUMPS
con14n_hp_in(n,bu,hp,h)$(feat_node('heat',n) AND theta_hp(n,bu,hp))..
        H_STO_IN_HP(n,bu,hp,h) 
        =E= 
        (H_HP_IN(n,bu,hp,h) ) * eta_heat_dyn(n,bu,hp) * ((temp_sink(n,bu,hp)+273.15)/(temp_sink(n,bu,hp) - temp_source(n,bu,hp,h)))
;

con14o_hp_maxin(n,bu,hp,h)$(feat_node('heat',n) AND theta_hp(n,bu,hp))..
        H_HP_IN(n,bu,hp,h)
        =L= 
        n_heat_p_in(n,bu,hp)
;

* (Hybrid) ELECTRIC HEATING
con14q_storage_elec_in(n,bu,hel,h)$(feat_node('heat',n) AND theta_storage(n,bu,hel) AND theta_elec(n,bu,hel) )..
        H_STO_IN_ELECTRIC(n,bu,hel,h) =E= H_ELECTRIC_IN(n,bu,hel,h)
;

con14r_storage_elec_maxin(n,bu,hel,h)$(feat_node('heat',n) AND theta_storage(n,bu,hel) AND theta_elec(n,bu,hel ))..
        H_ELECTRIC_IN(n,bu,hel,h)
        =L= 
        n_heat_p_in(n,bu,hel)
;

* HEAT STORAGE
con14t_storage_level(n,bu,hst,h)$(feat_node('heat',n) AND theta_storage(n,bu,hst) AND ord(h) > 1)..
        H_STO_LEV(n,bu,hst,h)
        =E=
        eta_heat_stat(n,bu,hst) * H_STO_LEV(n,bu,hst,h-1) + theta_hp(n,bu,hst)*H_STO_IN_HP(n,bu,hst,h) + theta_elec(n,bu,hst)*H_STO_IN_ELECTRIC(n,bu,hst,h) + theta_fossil(n,bu,hst) * H_STO_IN_FOSSIL(n,bu,hst,h)
        - H_STO_OUT(n,bu,hst,h) - H_DHW_STO_OUT(n,bu,hst,h)
;

con14u_storage_level_start(n,bu,hst,h)$(feat_node('heat',n) AND theta_storage(n,bu,hst) AND (ord(h) = 1 OR ord(h) = card(h)))..
         H_STO_LEV(n,bu,hst,h) =E= phi_heat_ini(n,bu,hst) * theta_storage(n,bu,hst)*n_heat_e(n,bu,hst)
;

* con14t_storage_level(n,bu,hst,h)$(feat_node('heat',n) AND theta_storage(n,bu,hst)..
*          H_STO_LEV(n,bu,hst,h)
*          =E=
*          eta_heat_stat(n,bu,hst) * H_STO_LEV(n,bu,hst,h--1) + theta_hp(n,bu,hst)*H_STO_IN_HP(n,bu,hst,h) + theta_elec(n,bu,hst)*H_STO_IN_ELECTRIC(n,bu,hst,h) + theta_fossil(n,bu,hst) * H_STO_IN_FOSSIL(n,bu,hst,h)
*          - H_STO_OUT(n,bu,hst,h) - H_DHW_STO_OUT(n,bu,hst,h)
* ;
* 
* *con14u_storage_level_start(n,bu,hst,h)$(feat_node('heat',n) AND theta_storage(n,bu,hst) AND (ord(h) = 1 OR ord(h) = card(h)))..
* *         H_STO_LEV(n,bu,hst,h) =E= phi_heat_ini(n,bu,hst) * theta_storage(n,bu,hst)*n_heat_e(n,bu,hst)
* *;

con14v_storage_maxlev(n,bu,hst,h)$(feat_node('heat',n) AND theta_storage(n,bu,hst))..
        H_STO_LEV(n,bu,hst,h) =L= n_heat_e(n,bu,hst)
;

$ontext
$offtext

* ---------------------------------------------------------------------------- *
***** extensive P2H2 module constraints *****
* ---------------------------------------------------------------------------- *

%P2H2_extensive%$ontext

* prod
h2_con2a(n,h2_tech,h2_tech_recon,h)$h2_tech_avail_set(n,h2_tech)..
         H2_N_PROD_CENT(n,h2_tech) =g= sum( h2_channel_wo_decent_set , H2_E_H2_IN(n,h2_tech,h2_channel_wo_decent_set,h) )
*                                         + h2_bidirect_sw(n,h2_tech_recon) * h2_bidirect_ratio(n,h2_tech_recon)  * sum( h2_channel_wo_decent_set , H2_E_RECON_OUT(n,h2_channel_wo_decent_set,h2_tech_recon,h)
*                                         $( h2_tech_recon_avail_set(n,h2_tech_recon) AND h2_recon_set(n,h2_channel_wo_decent_set) AND sameAs(h2_tech,h2_tech_recon) ) ) 
;

* Alternative formulation of "h2_con2a", gives the same result in a test ... (maybe needs another check)
*h2_con2a(n,h2_tech,h)$ h2_tech_avail_set(n,h2_tech)..
*         H2_N_PROD_CENT(n,h2_tech) =g= sum( h2_channel_wo_decent_set , H2_E_H2_IN(n,h2_tech,h2_channel_wo_decent_set,h) ) +
*sum( h2_channel_wo_decent_set ,
*sum( h2_tech_recon , h2_bidirect_sw(n,h2_tech_recon) * h2_bidirect_ratio(n,h2_tech_recon)  *
*H2_E_RECON_OUT(n,h2_channel_wo_decent_set,h2_tech_recon,h)$( h2_tech_recon_avail_set(n,h2_tech_recon) AND h2_recon_set(n,h2_channel_wo_decent_set) AND sameAs(h2_tech,h2_tech_recon) )
*)) ;

h2_con2b(n,h2_tech,h)$h2_tech_avail_set(n,h2_tech)..
         H2_N_PROD_DECENT(n,h2_tech) =g= H2_E_H2_IN(n,h2_tech,'fuel_decent',h) ;


h2_con3(n,h2_tech,h2_channel,h)$( h2_tech_avail_set(n,h2_tech) AND h2_channel_avail_set(n,h2_channel) )..
         H2_PROD_OUT(n,h2_tech,h2_channel,h) =e= h2_channel_avail_sw(n,h2_channel) * h2_tech_avail_sw(n,h2_tech) * h2_efficiency(n,h2_tech) * H2_E_H2_IN(n,h2_tech,h2_channel,h) ;

* aux_prod_site

h2_con4(n,h2_tech,h2_channel,h)$( h2_prod_aux_set(n,h2_tech,h2_channel) AND h2_tech_avail_set(n,h2_tech) AND h2_channel_avail_set(n,h2_channel) )..
         sum( h2_channel_alias , 1$( h2_prod_aux_set(n,h2_tech,h2_channel_alias) AND h2_tech_avail_set(n,h2_tech) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) * H2_N_PROD_AUX(n,h2_tech,h2_channel) =g= sum( h2_channel_alias , H2_PROD_AUX_IN(n,h2_tech,h2_channel_alias,h)$( h2_prod_aux_set(n,h2_tech,h2_channel_alias) AND h2_tech_avail_set(n,h2_tech) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) ;


h2_con5(n,h2_tech,h2_channel,h)$( h2_tech_avail_set(n,h2_tech) AND h2_channel_avail_set(n,h2_channel) )..
         H2_PROD_AUX_IN(n,h2_tech,h2_channel,h) + h2_bypass_1_sw(n,h2_tech,h2_channel) * H2_BYPASS_1(n,h2_tech,h2_channel,h) =e= H2_PROD_OUT(n,h2_tech,h2_channel,h) ;
h2_con6(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_PROD_AUX_OUT(n,h2_channel,h) =e=   sum( h2_tech , (1-h2_prod_aux_sw(n,h2_tech,h2_channel)*h2_eta_prod_aux(n,h2_tech,h2_channel))* H2_PROD_AUX_IN(n,h2_tech,h2_channel,h) )  ;

* hydration_liquefaction
h2_con7(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_HYD_LIQ_OUT(n,h2_channel,h) =e= (1-h2_hyd_liq_sw(n, h2_channel)* h2_eta_hyd(n,h2_channel)) * H2_PROD_AUX_OUT(n,h2_channel,h) ;


h2_con8(n,h2_channel,h)$( h2_hyd_liq_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         sum( h2_channel_alias , 1$( h2_hyd_liq_set(n,h2_channel_alias) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) *  H2_N_HYD_LIQ(n,h2_channel) =g= sum( h2_channel_alias , H2_PROD_AUX_OUT(n,h2_channel_alias,h)$( h2_hyd_liq_set(n,h2_channel_alias) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) ;

* prod_site_storage
h2_con9(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_HYD_LIQ_OUT(n,h2_channel,h) =e= H2_STO_P_IN(n,h2_channel,h) ;

h2_con10(n,h2_channel,h)$( h2_sto_p_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         sum( h2_channel_alias , 1$( h2_sto_p_set(n,h2_channel_alias) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) *  H2_N_STO(n,h2_channel) =g= sum( h2_channel_alias , H2_STO_P_L(n,h2_channel_alias,h)$( h2_sto_p_set(n,h2_channel_alias) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) ;

h2_con11a(n,h2_channel,h)$( ord(h) > 1 AND h2_channel_avail_set(n,h2_channel) )..
         h2_sto_p_sw(n,h2_channel) * H2_STO_P_L(n,h2_channel,h) =e= h2_sto_p_sw(n,h2_channel) * H2_STO_P_L(n,h2_channel,h-1) * ( 1 - h2_sto_p_eta_stat(n,h2_channel) ) + H2_STO_P_IN(n,h2_channel,h) - H2_STO_P_OUT(n,h2_channel,h) ;
h2_con11b(n,h2_channel,h)$( ord(h) = 1 AND h2_channel_avail_set(n,h2_channel) )..
         h2_sto_p_sw(n,h2_channel) * H2_STO_P_L(n,h2_channel,h) =e= h2_sto_p_sw(n,h2_channel) * H2_STO_P_L0(n,h2_channel) + H2_STO_P_IN(n,h2_channel,h) - H2_STO_P_OUT(n,h2_channel,h) ;
h2_con11c(n,h2_channel)$( h2_channel_avail_set(n,h2_channel) )..
        h2_sto_p_sw(n,h2_channel) * h2_sto_p_phi_ini(n,h2_channel) * sum( h2_channel_alias , 1$( h2_sto_p_set(n,h2_channel_alias) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) * H2_N_STO(n,h2_channel) =g= h2_sto_p_sw(n,h2_channel) * sum( h2_channel_alias , H2_STO_P_L0(n,h2_channel_alias)$( h2_sto_p_set(n,h2_channel_alias) AND h2_channel_avail_set(n,h2_channel_alias) AND h2_sto_p_type(n,h2_channel) = h2_sto_p_type(n,h2_channel_alias) ) ) ;

h2_con11d(n,h2_channel,h)$( ord(h) = card(h) AND h2_sto_p_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         h2_sto_p_sw(n,h2_channel) * H2_STO_P_L(n,h2_channel,h) =e= h2_sto_p_sw(n,h2_channel) * H2_STO_P_L0(n,h2_channel) ;

h2_con11e(n,h2_channel,h)$( h2_sto_p_set(n, h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         h2_sto_p_sw(n,h2_channel) * H2_STO_P_L(n,h2_channel,h) =g= H2_N_STO(n,h2_channel) * h2_sto_p_phi_min(n,h2_channel) ;

* aux_bftrans
h2_con12(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_AUX_PRETRANS_OUT(n,h2_channel,h) =e= (1-h2_aux_pretrans_sw(n,h2_channel)*h2_eta_aux_pretrans (n,h2_channel)) * (sum(h2_tech,h2_bypass_1_sw(n,h2_tech,h2_channel) * H2_BYPASS_1(n,h2_tech,h2_channel,h)) + H2_STO_P_OUT(n,h2_channel,h)) ;
h2_con13(n,h2_channel,h)$( h2_aux_pretrans_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_AUX_PRETRANS(n,h2_channel) =g=  H2_AUX_PRETRANS_OUT(n,h2_channel,h) /(1-h2_eta_aux_pretrans (n,h2_channel));

h2_con13b(n,h2_channel,h)$( h2_channel_avail_set(n,h2_channel) AND h2_trans_set(n,h2_channel) AND NOT h2_recon_set(n,h2_channel) )..
         H2_AUX_PRETRANS_OUT(n,h2_channel,h) =l= h2_LKW_cap(n,h2_channel)*( h2_mobility_sw(n,h2_channel)*h2_fill_station_nb(n)*H2_CHANNEL_SHARE(n,h2_channel) + h2_p2x_sw(n,h2_channel)*h2_fill_station_nb_p2x(n,h2_channel) );

*  Simplified_transporation
h2_con114(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_TRANS_OUT(n,h2_channel,h) =e=  H2_AUX_PRETRANS_OUT(n,h2_channel,h) * ( 1 - h2_trans_sw(n,h2_channel)*h2_trans_eta(n,h2_channel) ) ;
h2_con115(n,h2_channel,h)$( h2_trans_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_TRANS(n,h2_channel) =g= H2_AUX_PRETRANS_OUT(n,h2_channel,h) ;

* Time_consuming_transportation
h2_con14a(n,h2_channel,h)$( h2_channel_avail_set(n,h2_channel) AND ( ord(h) > h2_trans_load_time(n,h2_channel) + h2_trans_unload_time(n,h2_channel) + h2_trans_dist(n,h2_channel)/ (2*50) ) AND h2_trans_set(n,h2_channel) )..
         H2_TRANS_OUT(n,h2_channel,h) =e= H2_AUX_PRETRANS_OUT(n,h2_channel,h - (h2_trans_load_time(n,h2_channel) + h2_trans_unload_time(n,h2_channel) + (h2_trans_dist(n,h2_channel)/ ( 2 * 50) ) ) ) * power( 1 - h2_trans_sw(n,h2_channel)*h2_trans_eta(n,h2_channel), max (0,h2_trans_dist(n,h2_channel)/ (2*50)) )  ;
h2_con14b(n,h2_channel,h)$ ( h2_channel_avail_set(n,h2_channel) AND ( ord(h) <= h2_trans_load_time(n,h2_channel) + h2_trans_unload_time(n,h2_channel)+ h2_trans_dist(n,h2_channel)/ (2*50) ) AND h2_trans_set(n,h2_channel) ) ..
         H2_TRANS_OUT(n,h2_channel,h) =e= H2_AUX_PRETRANS_OUT(n,h2_channel, h + ( card (h) -   h2_trans_load_time(n,h2_channel) - h2_trans_unload_time(n,h2_channel) - (h2_trans_dist(n,h2_channel) / ( 50 * 2 )) ) ) * power( (1 - h2_trans_sw(n,h2_channel) *  h2_trans_eta(n,h2_channel)),max(0,h2_trans_dist(n,h2_channel)/ (2*50)) ) ;

h2_con14c(n,h2_channel,h)$(h2_channel_avail_set(n,h2_channel) AND NOT h2_trans_set(n,h2_channel)) ..
         H2_TRANS_OUT(n,h2_channel,h) =e= H2_AUX_PRETRANS_OUT(n,h2_channel,h)  ;

h2_con15a(n,h2_channel,h)$( h2_trans_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_AVAI_TRANS(n,h2_channel,h) =g=   H2_AUX_PRETRANS_OUT(n,h2_channel,h) ;
h2_con15b(n,h2_channel,h)$( h2_trans_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel))..
         H2_N_AVAI_TRANS(n,h2_channel,h) =e= H2_N_TRANS(n,h2_channel) - sum( hh , H2_AUX_PRETRANS_OUT(n,h2_channel,hh)$( ( ord(hh)<ord(h) AND ord(h)-ord(hh) < (h2_trans_load_time(n,h2_channel) + h2_trans_unload_time(n,h2_channel) + (h2_trans_dist(n,h2_channel)/50)) ) OR ( ord(h)-1 < (h2_trans_load_time(n,h2_channel) + h2_trans_unload_time(n,h2_channel) + (h2_trans_dist(n,h2_channel)/50)) AND card(hh)-ord(hh) < - ( ord (h) -1 - h2_trans_load_time(n,h2_channel) - h2_trans_unload_time(n,h2_channel) - (h2_trans_dist(n,h2_channel)/50) ) ) ) ) ;

* aux_bflp_storage
h2_con_1(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_AUX_BFLP_STO_OUT(n,h2_channel,h) =e= (1-h2_aux_bflp_sto_sw(n,h2_channel) * h2_eta_aux_bflp_sto (n,h2_channel)) * H2_AUX_BFLP_STO_IN(n,h2_channel,h) ;
h2_con_2(n,h2_channel,h)$( h2_aux_bflp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_AUX_BFLP_STO(n,h2_channel) =g= H2_AUX_BFLP_STO_IN(n,h2_channel,h) ;

h2_con17e(n,h2_channel,h)$( h2_channel_avail_set(n,h2_channel) )..
         H2_AUX_BFLP_STO_IN(n,h2_channel,h) =e= H2_TRANS_OUT(n,h2_channel,h) - h2_bypass_2_sw(n,h2_channel) * H2_BYPASS_2(n,h2_channel,h)  ;

* lp_storage
h2_con16a(n,h2_channel,h)$( h2_lp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_LP_STO(n,h2_channel) =g= H2_LP_STO_L(n,h2_channel,h) ;
h2_con16b(n,h2_channel,h)$( h2_lp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) AND NOT h2_recon_set(n,h2_channel)  )..
         H2_N_LP_STO(n,h2_channel) =l= ( h2_mobility_sw(n,h2_channel)*h2_fill_station_nb(n)*H2_CHANNEL_SHARE(n,h2_channel) + h2_p2x_sw(n,h2_channel)*h2_fill_station_nb_p2x(n,h2_channel) ) * h2_lp_sto_station_cap(n,h2_channel) ;
h2_con16c(n,h2_channel,h)$( h2_lp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) AND h2_recon_set(n,h2_channel)  )..
         H2_N_LP_STO(n,h2_channel) =l= h2_lp_sto_station_cap(n,h2_channel) ;

h2_con17a(n,h2_channel,h)$( ord(h) > 1 AND h2_channel_avail_set(n,h2_channel) )..
         h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L(n,h2_channel,h) =e= h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L(n,h2_channel,h-1) *( 1 - h2_lp_sto_eta_stat(n,h2_channel) ) +  H2_AUX_BFLP_STO_OUT(n,h2_channel,h) - H2_LP_STO_OUT(n,h2_channel,h) ;
h2_con17b(n,h2_channel,h)$( ord(h) = 1 AND h2_channel_avail_set(n,h2_channel) )..
         h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L(n,h2_channel,h) =e= h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L0(n,h2_channel) +  H2_AUX_BFLP_STO_OUT(n,h2_channel,h) - H2_LP_STO_OUT(n,h2_channel,h) ;
h2_con17c(n,h2_channel)$( h2_channel_avail_set(n,h2_channel) )..
         h2_lp_sto_sw(n,h2_channel) * h2_lp_sto_phi_ini(n,h2_channel) * H2_N_LP_STO(n,h2_channel) =g= h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L0(n,h2_channel) ;
h2_con17d(n,h2_channel,h)$( ord(h) = card(h) AND h2_lp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L(n,h2_channel,h) =e= h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L0(n,h2_channel) ;

h2_con17g(n,h2_channel,h)$( h2_lp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         h2_lp_sto_sw(n,h2_channel) * H2_LP_STO_L(n,h2_channel,h)=g= H2_N_LP_STO(n,h2_channel) * h2_lp_sto_phi_min(n,h2_channel) ;

* dehydration_evaporation
h2_con18(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_DEHYD_EVAP_OUT(n,h2_channel,h) =e= (1-h2_dehyd_evap_sw(n,h2_channel) * h2_eta_dehyd_evap(n,h2_channel)) * H2_LP_STO_OUT(n,h2_channel,h) ;
h2_con19(n,h2_channel,h)$( h2_dehyd_evap_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_DEHYD_EVAP(n,h2_channel) =g= H2_LP_STO_OUT(n,h2_channel,h) ;

* aux_bfMP_storage
h2_con_7(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_AUX_BFMP_STO_OUT(n,h2_channel,h) =e= (1-h2_aux_bfMP_sto_sw(n,h2_channel)*h2_eta_aux_bfmp_sto(n,h2_channel)) * H2_DEHYD_EVAP_OUT(n,h2_channel,h) ;
h2_con_8(n,h2_channel,h)$( h2_aux_bfmp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_AUX_BFMP_STO(n,h2_channel) =g= H2_DEHYD_EVAP_OUT (n,h2_channel,h) ;

* MP_storage
h2_con_3(n,h2_channel,h)$( h2_MP_sto_set(n,h2_channel)AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_MP_STO(n,h2_channel) =g= H2_MP_STO_L(n,h2_channel,h) ;
h2_con_4a(n,h2_channel,h)$( ord(h) > 1 AND h2_channel_avail_set(n,h2_channel) )..
         h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L(n,h2_channel,h) =e= h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L(n,h2_channel,h-1) *( 1 - h2_MP_sto_eta_stat(n,h2_channel) ) +   H2_AUX_BFMP_STO_OUT(n,h2_channel,h) - H2_MP_STO_OUT(n,h2_channel,h) ;
h2_con_4b(n,h2_channel,h)$( ord(h) = 1 AND h2_channel_avail_set(n,h2_channel) )..
         h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L(n,h2_channel,h) =e= h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L0(n,h2_channel) +   H2_AUX_BFMP_STO_OUT(n,h2_channel,h) - H2_MP_STO_OUT(n,h2_channel,h) ;
h2_con_4c(n,h2_channel)$( h2_channel_avail_set(n,h2_channel) )..
         h2_MP_sto_sw(n,h2_channel) * h2_MP_sto_phi_ini(n,h2_channel) * H2_N_MP_STO(n,h2_channel) =g= h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L0(n,h2_channel) ;
h2_con_4d(n,h2_channel,h)$( ord(h) = card(h) AND h2_MP_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L(n,h2_channel,h) =e= h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L0(n,h2_channel) ;
h2_con_4e(n,h2_channel,h)$( h2_MP_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         h2_MP_sto_sw(n,h2_channel) * H2_MP_STO_L(n,h2_channel,h)=g= H2_N_MP_STO(n,h2_channel) * h2_MP_sto_phi_min(n,h2_channel) ;

h2_con_4f(n,h2_channel,h)$( h2_MP_sto_set(n,h2_channel)AND h2_channel_avail_set(n,h2_channel) AND NOT h2_recon_set(n,h2_channel))..
         H2_N_MP_STO(n,h2_channel) =l= ( h2_mobility_sw(n,h2_channel)*h2_fill_station_nb(n)*H2_CHANNEL_SHARE(n,h2_channel) + h2_p2x_sw(n,h2_channel)*h2_fill_station_nb_p2x(n,h2_channel) ) * h2_MP_sto_station_cap(n,h2_channel) ;

h2_con_4g(n,h2_channel,h)$( h2_MP_sto_set(n,h2_channel)AND h2_channel_avail_set(n,h2_channel)AND h2_recon_set(n,h2_channel))..
         H2_N_MP_STO(n,h2_channel) =l=  h2_MP_sto_station_cap(n,h2_channel) ;

* aux_bffilling_storage
h2_con21(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_AUX_BFHP_STO_OUT(n,h2_channel,h) =e= (1-h2_aux_bfhp_sto_sw(n,h2_channel)* h2_eta_aux_bfhp_sto(n,h2_channel)) *  H2_AUX_BFHP_STO_IN(n,h2_channel,h);

h2_con21b(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
H2_AUX_BFHP_STO_IN(n,h2_channel,h) =e= H2_MP_STO_OUT(n,h2_channel,h) + h2_bypass_2_sw(n,h2_channel) * H2_BYPASS_2(n,h2_channel,h);

h2_con22(n,h2_channel,h)$( h2_aux_bfhp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_AUX_BFHP_STO(n,h2_channel) =g= H2_AUX_BFHP_STO_IN(n,h2_channel,h) ;

* filling_storage
h2_con24(n,h2_channel,h)$( h2_hp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_HP_STO(n,h2_channel) =g= H2_HP_STO_L(n,h2_channel,h) ;
h2_con25a(n,h2_channel,h)$( ord(h) > 1 AND h2_channel_avail_set(n,h2_channel) )..
          h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L(n,h2_channel,h) =e= h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L(n,h2_channel,h-1) * ( 1 - h2_hp_sto_eta_stat(n,h2_channel) ) + H2_AUX_BFHP_STO_OUT(n,h2_channel,h) - H2_HP_STO_OUT(n,h2_channel,h) ;
h2_con25b(n,h2_channel,h)$( ord(h) = 1 AND h2_channel_avail_set(n,h2_channel) )..
         h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L(n,h2_channel,h) =e= h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L0(n,h2_channel) +H2_AUX_BFHP_STO_OUT(n,h2_channel,h) - H2_HP_STO_OUT(n,h2_channel,h) ;
h2_con25c(n,h2_channel,h)$( h2_hp_sto_set(n,h2_channel) AND ord(h) = card(h) AND h2_channel_avail_set(n,h2_channel) )..
         h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L(n,h2_channel,h) =e= h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L0(n,h2_channel) ;
h2_con25d(n,h2_channel,h)$( h2_hp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         h2_hp_sto_sw(n,h2_channel) * H2_HP_STO_L(n,h2_channel,h) =g= H2_N_HP_STO(n,h2_channel) * h2_hp_sto_phi_min(n,h2_channel) ;

h2_con25e(n,h2_channel,h)$( NOT h2_recon_set(n,h2_channel) AND h2_hp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_HP_STO(n,h2_channel) =l= ( h2_mobility_sw(n,h2_channel)*h2_fill_station_nb(n)*H2_CHANNEL_SHARE(n,h2_channel) + h2_p2x_sw(n,h2_channel)*h2_fill_station_nb_p2x(n,h2_channel) ) * h2_hp_sto_station_cap(n,h2_channel) ;

h2_con25f(n,h2_channel,h)$(  h2_recon_set(n,h2_channel) AND h2_hp_sto_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_HP_STO(n,h2_channel) =l= h2_hp_sto_station_cap(n,h2_channel) ;

* aux_bffuel
h2_con_5(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         H2_AUX_BFFUEL_OUT(n,h2_channel,h) =e= (1-h2_aux_bffuel_sw(n,h2_channel)*h2_eta_aux_bffuel (n,h2_channel)) *  H2_HP_STO_OUT(n,h2_channel,h) ;
h2_con_6(n,h2_channel,h)$( h2_aux_bffuel_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_AUX_BFFUEL(n,h2_channel) =g= H2_HP_STO_OUT(n,h2_channel,h) ;

* matching demand and supply
h2_con28a(n,h,h2_channel)$( h2_mobility_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_AUX_BFFUEL_OUT(n,h2_channel,h) =e= H2_CHANNEL_SHARE(n,h2_channel)* d_h2_extensive(n,h) ;
h2_con28b(n)..
         sum( h2_channel , H2_CHANNEL_SHARE(n,h2_channel)$( h2_mobility_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) ) ) =e= 1 * 1$( sum(h, d_h2_extensive(n,h) ) <> 0  )  ;
* The condition: 1$( sum(h, d_h2_extensive(n,h) ) <> 0  ) allows for turning all H2 channels off if H2 demand is zero.

h2_con29(n,h,h2_channel)$( h2_p2x_set(n,h2_channel) AND h2_channel_avail_set(n,h2_channel) )..
         H2_AUX_BFFUEL_OUT(n,h2_channel,h) =e= d_p2x(n,h,h2_channel) * h2_channel_avail_sw(n,h2_channel) * h2_p2x_sw(n,h2_channel) ;

* aux_recon_site

h2_con31(n,h2_channel,h2_tech_recon,h)$( h2_recon_aux_set(n,h2_channel,h2_tech_recon) AND h2_channel_avail_set(n,h2_channel) )..
         H2_N_RECON_AUX(n,h2_channel,h2_tech_recon) =g= H2_RECON_AUX_OUT(n,h2_channel,h2_tech_recon,h) / (1-h2_eta_recon_aux(n,h2_channel,h2_tech_recon));
h2_con32(n,h2_channel,h)$h2_channel_avail_set(n,h2_channel)..
         sum( h2_tech_recon , (H2_RECON_AUX_OUT(n,h2_channel,h2_tech_recon,h)/(1-h2_recon_aux_sw(n,h2_channel,h2_tech_recon)*h2_eta_recon_aux(n,h2_channel,h2_tech_recon)))$h2_tech_recon_avail_set(n,h2_tech_recon) ) =e=  H2_AUX_BFFUEL_OUT(n,h2_channel,h)$( h2_channel_avail_set(n,h2_channel) AND h2_recon_set(n,h2_channel) ) ;

* recon
* h2_con33(n,h2_tech_recon,h)$(h2_tech_recon_avail_set(n,h2_tech_recon) AND NOT h2_bi_recon_set(n,h2_tech_recon) ) ..  "Old one with h2_bi_recon_set"
h2_con33(n,h2_tech_recon,h)$(h2_tech_recon_avail_set(n,h2_tech_recon)) ..
         H2_N_RECON_extensive(n,h2_tech_recon) =g= sum( h2_channel , H2_E_RECON_OUT(n,h2_channel,h2_tech_recon,h) $( h2_tech_recon_avail_set(n,h2_tech_recon) AND h2_channel_avail_set(n,h2_channel) AND h2_recon_set(n,h2_channel) ) ) ;
h2_con34(n,h2_channel,h2_tech_recon,h)..
         H2_E_RECON_OUT(n,h2_channel,h2_tech_recon,h) =e= h2_channel_avail_sw(n,h2_channel) * h2_recon_sw(n,h2_channel) * h2_tech_recon_sw(n,h2_tech_recon) * h2_recon_efficiency(n,h2_tech_recon) * H2_RECON_AUX_OUT(n,h2_channel,h2_tech_recon,h) ;

$ontext
$offtext



********************************************************************************

%DIETERgms%$include "%MODELDIR%dieterpy_4_developnewconstr.gms"

********************************************************************************
***** MODEL *****
********************************************************************************

model DIETER /
obj

con1a_bal

con2a_loadlevel
con2b_loadlevelstart

con3a_maxprod_dispatchable
con3b_maxprod_res

* Storage constraints
con4a1_stolev
con4a2_stolev_ini
con4a3_stolev_last
con4b_stolev_max
con4c_stolev_min
con4d_maxin_sto
con4e_maxout_sto
con4f_maxin_lev
con4g_maxout_lev
con4h_EtoP

con5a_minRES
con5b_max_energy

%DSM%$ontext
con6a_DSMcurt_duration_max
con6b_DSMcurt_max

con7a_DSMshift_upanddown
con7b_DSMshift_granular_max
con7c_DSM_distrib_up
con7d_DSM_distrib_do
*con_7e_DSMshift_recovery
$ontext
$offtext

con8a1_min_I_power
con8a2_max_I_power
con8b1_min_I_sto_e
con8b2_max_I_sto_e
con8c1_min_I_sto_p_in
con8c2_max_I_sto_p_in
con8d1_min_I_sto_p_out
con8d2_max_I_sto_p_out
%DSM%$ontext
con8e_max_I_dsm_cu
con8f_max_I_dsm_shift_pos
$ontext
$offtext
con8j1a_min_I_ntc_ex
con8j1b_max_I_ntc_ex
con8j2a_min_I_ntc_im
con8j2b_max_I_ntc_im
%P2H2P_parsimonious%$ontext
con8k1_min_I_h2_ely_p
con8k2_max_I_h2_ely_p
con8l1_min_I_h2_sto_e
con8l2_max_I_h2_sto_e
con8m1_min_I_h2_recon_p
con8m2_max_I_h2_recon_p

con9a_h2_ely_conv
con9b_h2_ely_conv_max
con9c_h2_sto_lev_start
con9d_h2_sto_lev
con9e_h2_sto_lev_end
con9f_h2_sto_lev_max
con9g_h2_sto_lev_min
con9h_h2_recon
con9i_h2_recon_max
$ontext
$offtext

%EV%$ontext
 con10a_ev_ed
%EV_EXOG% con10b_ev_chargelev_start
 con10c_ev_chargelev
 con10d_ev_chargelev_max
%EV_EXOG% con10e_ev_maxin
%EV_EXOG% con10f_ev_maxout
%EV_EXOG% con10g_ev_chargelev_ending
%EV_EXOG% con10h_ev_maxin_lev
%EV_EXOG% con10i_ev_maxout_lev
$ontext
$offtext
%EV%$ontext
%EV%$ontext
%EV_EXOG%$ontext
 con10k_ev_exog
$ontext
$offtext

%prosumage%$ontext
con8g_max_pro_res
con8h_max_pro_sto_e
con8i_max_sto_pro_p
con11a_pro_distrib
con11b_pro_balance
con11c_pro_selfcon
con11d_pro_stolev_PRO2PRO
con11e_pro_stolev_PRO2M
con11f_pro_stolev_M2PRO
con11g_pro_stolev_M2M
con11h_1_pro_stolev_start_PRO2PRO
con11h_2_pro_stolev_start_PRO2M
con11h_3_pro_stolev_start_M2PRO
con11h_4_pro_stolev_start_M2M
con11i_pro_stolev
con11j_pro_stolev_max
con11k_pro_maxin_sto
con11l_pro_maxout_sto
con11m_pro_maxout_lev
con11n_pro_maxin_lev
con11o_pro_ending
$ontext
$offtext

con12a_max_f
con12b_min_f


%heat%$ontext

con_heat_bal
con_heat_sto_lev
con_heat_sto_lev_ini
con_heat_sto_lev_end
con_heat_sto_lev_max
con_heat_elec_in
con_heat_elec_in_max
con_heat_n_sto_out
con_heat_n_sto_in
con_heat_n_sto_e
con_heat_n_elec_in

* old heat module
*con14a_heat_balance
*con14b_dhw_balance
*con14c_sets_level
*con14d_sets_level_start
*con14e_sets_maxin
*con14f_sets_maxout
*con14h_sets_maxlev
*
*con14i_sets_aux_dhw_level
*con14j_sets_aux_dhw_level_start
*con14k_sets_aux_dhw_maxin
*con14m_sets_aux_dhw_maxlev
*
*con14n_hp_in
*con14o_hp_maxin
*con14q_storage_elec_in
*con14r_storage_elec_maxin
*con14t_storage_level
*con14u_storage_level_start
*con14v_storage_maxlev
$ontext
$offtext

%P2H2_extensive%$ontext
* prod
h2_con2a
h2_con2b
h2_con3

* aux_prod_site
h2_con4
h2_con5
h2_con6

* hydration_liquefaction
h2_con7
h2_con8

* prod_site_storage
h2_con9
h2_con10
h2_con11a
h2_con11b
h2_con11c
h2_con11d
h2_con11e

* aux_bftans
h2_con12
h2_con13
h2_con13b

* aux_bflp_storage
h2_con_1
h2_con_2

* lp_storage
h2_con16a
h2_con16b
h2_con16c
h2_con17a
h2_con17b
h2_con17c
h2_con17d
h2_con17e

h2_con17g

* dehydration_evaporation
h2_con18
h2_con19

* aux_bfMP_storage
h2_con_7
h2_con_8

* MP_storage
h2_con_3
h2_con_4a
h2_con_4b
h2_con_4c
h2_con_4d
h2_con_4e
h2_con_4f
h2_con_4g

* aux_bffilling_storage
h2_con21
h2_con21b
h2_con22

* filling_storage
h2_con24
h2_con25a
h2_con25b
h2_con25c
h2_con25d
h2_con25e
h2_con25f

* aux_bffuel
h2_con_5
h2_con_6

* matching demand and supply
h2_con28a
h2_con28b
h2_con29

* aux_recon_site
h2_con31
h2_con32

* recon
h2_con33
h2_con34

$ontext
$offtext

%P2H2_extensive%$ontext
%Time_consuming_transportation%$ontext
* transportation
h2_con14a
h2_con14b
h2_con14c
h2_con15a
h2_con15b

$ontext
$offtext

%P2H2_extensive%$ontext
%Simplified_transporation%$ontext
* transportation
h2_con114
h2_con115

$ontext
$offtext

%DIETERgms%$include "%MODELDIR%dieterpy_5_includenewconstr.gms"

/;
