import os
from dieterpy.scripts.runopt import main
from dieterpy.config import settings

settings.PROJECT_DIR_ABS = os.getcwd()
settings.update_changes()

main(f'project_variables.csv')