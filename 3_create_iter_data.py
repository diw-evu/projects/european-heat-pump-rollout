#%% Import libraries ###############################################################################

import os
import pandas as pd
import numpy as np

# Import data
from helper import * 

#%% Define functions ###############################################################################

def create_iter_heat_data(
    input_heat=None, input_cop=None, countries=None, techs=None, buildings=None, years=None, 
    year_type=None):
    
    if input_heat is not None:

        # Rename values
        input_heat[['heat_sink','heat_building']] = input_heat['type'].str.split('_', 1, expand=True)
        input_heat['heat_sink']     = input_heat['heat_sink'].replace(
            {"floor": "space", "radiator":"space"})
        input_heat['heat_building'] = input_heat['heat_building'].replace(
            {"COM": "com", "SFH": "sfh", "MFH": "mfh"})
        
        # Filter
        input_heat = input_heat.query(
            "n == @countries & heat_building == @buildings & year == @years")
        
        # Create parameter
        input_heat['parameter'] = "heat_demand('" + input_heat['n'] + "','" + input_heat['heat_building'] + "','" + input_heat['heat_sink'] + "',h)"
        
        # Create identifer
        input_heat['identifier'] = "heat_demand_" + input_heat['heat_building'] + "_" + input_heat['heat_sink'] + "_" + input_heat['n']
        
        # Create scenario
        input_heat['scenario'] = "y" + input_heat['year'].astype(str)

        # Select variables
        input_heat = input_heat[['parameter','identifier','scenario','year','h','value']].reset_index(drop=True)
    
    if input_cop is not None:
        
        input_cop[['heat_type_aux','heat_sink']] = input_cop['type'].str.split('_', 1, expand=True)
        input_cop['heat_sink'] = input_cop['heat_sink'].replace({"floor": "space", "radiator":"space"})
        input_cop['type']      = input_cop['type'].replace({
            'ASHP_floor'   : 'hp_air_floor',
            'ASHP_radiator': 'hp_air_radiator',
            'ASHP_water'   : 'hp_air_water',
            'GSHP_floor'   : 'hp_ground_floor',
            'GSHP_radiator': 'hp_ground_radiator',
            'GSHP_water'   : 'hp_ground_water',
            'WSHP_floor'   : 'hp_water_floor',
            'WSHP_radiator': 'hp_water_radiator',
            'WSHP_water'   : 'hp_water_water',
            'direct_space' : 'direct',
            'direct_water' : 'direct'
            })
        
        # Filter
        input_cop = input_cop.query(
            "n == @countries & type == @techs & year == @years")
        
        # Create parameter
        input_cop['parameter'] = "heat_cop('" + input_cop['n'] + "','" + input_cop['heat_sink'] + "','" + input_cop['type'] + "',h)"
        
        # Create identifer
        input_cop['identifier'] = "heat_cop_" + input_cop['heat_sink'] + "_" + input_cop['type'] + "_" + input_cop['n']
        
        # Create scenario
        input_cop['scenario'] = "y" + input_cop['year'].astype(str)
        
        # Select variables 
        input_cop = input_cop[['parameter','identifier','scenario','year','h','value']].reset_index(drop=True)
    
    else:
        Print("No data provided")
    
    # Chose correct data
    if input_heat is None:
        merged = input_cop
    elif input_cop is None:
        merged = input_heat
    else:
        merged = pd.concat([input_heat,input_cop])
    
    # Create alternative 
    merged['year'] = merged['year'].astype(int)
    merged['h']    = merged['h'].astype(int)
    
    # Create new index
    if year_type == "jan-dec":
        merged['h_new'] = 't' + merged['h'].astype(str).str.zfill(4)
        
    if year_type == "jul-jun":
        # Define alternative year 
        merged['year2']   = np.where(merged['h']<=4344, merged['year'] - 1, merged['year'])
        merged['year_ac'] = merged['year2'].map(str) + "-" +(merged['year2']+1).map(str)
        merged.drop(columns=['year2'], inplace = True)
    
        # Define alternative h-index (July-June)
        merged['h2'] = merged['h'] - 4344
        merged['h2'] = np.where(merged['h2'] <= 0, merged['h2'] + 8760, merged['h2'])
        merged['h_new'] = 't' + merged['h2'].astype(str).str.zfill(4)
        
    else:
        raise Exception("Define type year") 
    
    # Make wide
    merged_wide = merged.pivot(index=['parameter','identifier','scenario'], columns='h_new', values='value')
    
    return merged_wide

def create_base_iter_data(dict_df, selected_countries, selected_years, year_type):
    
    # Filter data
    sel_demand    = dict_df["demand"].query("year == @selected_years & n == @selected_countries") 
    sel_pv        = dict_df["pv"].query("year == @selected_years & n == @selected_countries")
    sel_wind_on   = dict_df["wind_on"].query("year == @selected_years & n == @selected_countries") 
    sel_wind_off  = dict_df["wind_off"].query("year == @selected_years & n == @selected_countries") 
    sel_rsvr      = dict_df["rsvr"].query("year == @selected_years & n == @selected_countries") 
    sel_phs       = dict_df["phs_open"].query("year == @selected_years & n == @selected_countries") 
    sel_ror       = dict_df["ror"].query("year == @selected_years & n == @selected_countries") 

    # Add identifier
    sel_demand['identifier']   = 'load_'        + sel_demand['n']
    sel_pv['identifier']       = 'pv_'          + sel_pv['n']
    sel_wind_on['identifier']  = 'wind_on_'     + sel_wind_on['n']
    sel_wind_off['identifier'] = 'wind_off_'    + sel_wind_off['n']
    sel_rsvr['identifier']     = 'rsvr_'        + sel_rsvr['n']
    sel_phs['identifier']      = 'phs_open_'    + sel_phs['n']
    sel_ror['identifier']      = 'ror_'         + sel_ror['n']
    
    # Add parameter
    sel_demand['parameter']   = "d('"           + sel_demand['n'] + "',h)"
    sel_pv['parameter']       = "phi_res('"     + sel_pv['n'] + "','pv',h)"
    sel_wind_on['parameter']  = "phi_res('"     + sel_wind_on['n'] + "','wind_on',h)"
    sel_wind_off['parameter'] = "phi_res('"     + sel_wind_off['n'] + "','wind_off',h)"
    sel_rsvr['parameter']     = "sto_flow_in('" + sel_rsvr['n'] + "','rsvr',h)"
    sel_phs['parameter']      = "sto_flow_in('" + sel_phs['n'] + "','phs_open',h)"
    sel_ror['parameter']      = "phi_res('"     + sel_ror['n'] + "','ror',h)"  

    # Merge data
    merged = pd.concat(
        [sel_demand,sel_pv,sel_wind_on,sel_wind_off,sel_rsvr,sel_phs,sel_ror],
        ignore_index = True)
    
    # Create alternative 
    merged['year'] = merged['year'].astype(int)
    merged['h']    = merged['h'].astype(int)
    
    # Create new index
    if year_type == "jan-dec":
        merged['h_new'] = 't' + merged['h'].astype(str).str.zfill(4)
        
    if year_type == "jul-jun":
        # Define alternative year 
        merged['year2']   = np.where(merged['h']<=4344, merged['year'] - 1, merged['year'])
        merged['year_ac'] = merged['year2'].map(str) + "-" +(merged['year2']+1).map(str)
        merged.drop(columns=['year2'], inplace = True)
    
        # Define alternative h-index (July-June)
        merged['h2'] = merged['h'] - 4344
        merged['h2'] = np.where(merged['h2'] <= 0, merged['h2'] + 8760, merged['h2'])
        merged['h_new'] = 't' + merged['h2'].astype(str).str.zfill(4)
        
    else:
        raise Exception("Define type year") 
        
    # Add scenario
    merged["scenario"] = "y" + merged['year'].astype(str)
    
    merged = merged[['parameter','identifier','scenario','h_new','value']]
    
    merged_wide = merged.pivot(index=['parameter','identifier','scenario'], columns='h_new', values='value')
    
    return merged_wide

#%% Import data ####################################################################################

cop = pd.read_feather(os.path.join("data","heat","cop.feather"))
hd  = pd.read_feather(os.path.join("data","heat","hd.feather"))

#%% Choose data to be processed ####################################################################

years_iter          = [2009,2010,2011,2012,2013,2014]
n_selected          = ["AT","BE","CH","DE","DK","FR","IT","LU","NL"]
heat_tech_sel       = ['hp_air_radiator','hp_air_water']
heat_buildings_sel  = ['sfh','mfh','com']

#%% Get heat iter data #############################################################################

heat_iter_data = create_iter_heat_data(
    input_heat=hd, input_cop=cop, countries=n_selected, techs=heat_tech_sel, 
    buildings=heat_buildings_sel, years=years_iter,
    year_type = "jul-jun")

#%% Demand and capa factors ########################################################################

base_iter_data = create_base_iter_data(
    dict_df = data_dict, 
    selected_countries = n_selected, 
    selected_years = years_iter,
    year_type = "jul-jun")
    
#%% Export iteration data ##########################################################################

export_iter_data = pd.concat([heat_iter_data,base_iter_data])

export_iter_data.to_csv(
    os.path.join("project_files","iterationfiles","iteration_data_wide","par_scenario_iter_data.csv"))

#%% Export sets ####################################################################################

# Identifier
set_identifier1 = (heat_iter_data.reset_index())['identifier'].unique().tolist()
set_identifier2 = (base_iter_data.reset_index())['identifier'].unique().tolist()

set_identifier = set_identifier1 + set_identifier2

set_identifier.insert(0,'identifier')
pd.Series(set_identifier).to_csv(
    os.path.join("project_files","iterationfiles","iteration_data_wide","set_identifier.csv"),
    header = False, index = False)

# scenario
set_scenario = (heat_iter_data.reset_index())['scenario'].unique().tolist()
set_scenario.insert(0,'scenario')
pd.Series(set_scenario).to_csv(
    os.path.join("project_files","iterationfiles","iteration_data_wide","set_scenario.csv"),
    header = False, index = False)

#%% ################################################################################################

