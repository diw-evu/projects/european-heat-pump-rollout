

***************  UPLOAD TIME-CONSTANT SETS AND PARAMETERS  *********************

***********************************
***** Definition via Upload *******
***********************************

***** Define country set **********

* with iteration
%iter_countries_switch_on%$ontext

$include "%countries_lines_gms%"
$ontext
$offtext

* without iteration
%iter_countries_switch_off%$ontext
$GDXin "%data_input_gdx%"
$load n l
;
$ontext
$offtext

************************************

$GDXin "%data_input_gdx%"
$load tech headers_tech tech_dispatch tech_res_con
$load sto headers_sto dsm headers_dsm dsm_type
$load technology_data_upload storage_data dsm_data_upload
$load headers_topology topology_data
$load inc
$load ev headers_ev ev_data
$load headers_prosumage_generation headers_prosumage_storage prosumage_data_generation prosumage_data_storage
*$load bu ch heat_storage heat_hp heat_elec heat_fossil headers_heat heat_data_upload
$load heat_building heat_tech heat_sink headers_heat heat_data
$load h2_ely h2_sto h2_recon headers_h2_ely headers_h2_sto headers_h2_recon h2_ely_data_upload h2_sto_data_upload h2_recon_data_upload
$load h2_tech h2_channel h2_tech_recon headers_h2_parameters_table1 headers_h2_parameters_table2 headers_h2_parameters_table3 headers_h2_parameters_table4 headers_h2_parameters_table5 headers_h2_parameters_table6 headers_h2_parameters_table7 headers_h2_parameters_table8 headers_h2_parameters_table9 headers_h2_parameters_table10 h2_parameter_data_table1 h2_parameter_data_table2 h2_parameter_data_table3 h2_parameter_data_table4 h2_parameter_data_table5 h2_parameter_data_table6 h2_parameter_data_table7 h2_parameter_data_table8 h2_parameter_data_table9 h2_parameter_data_table10
$load headers_nodes, headers_scalar, nodes_data_upload, scalar_data_upload
;

***************  UPLOAD TIME-SERIES SETS AND PARAMETERS  ***********************

***********************************
******* Definition end_hour *******
***********************************

******* Define custom h set *******
%end_hour%$ontext

Set h "Hours" / %h_set% / ;

$ontext
$offtext
***********************************

$GDXin "%data_input_gdx%"
%end_hour%$load h
$load headers_time time_data_upload
$load headers_time_ev ev_time_data_upload
*$load dh_upload
*$load temp_source_upload
*$load d_dhw_upload
$load heat_demand heat_cop
$load headers_h2_time_data h2_time_data_upload
$load h2_time_data
$load h2_p2x_time_data
;

* we need all global features as set-coordinates
set
features /%feature_set%/
;

parameter

feat_node(features,n)
;

$GDXin "%feat_node_gdx%"
$load feat_node
;
