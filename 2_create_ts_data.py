#%% Libraries ######################################################################################

import pandas as pd
import numpy as np
import os
import openpyxl
pd.options.plotting.backend = "plotly"

# Import data

from helper import * 

#%% Create base time series sheet ##################################################################

# Select whether one- or two-year period to be used
period_length = 1

# Select whether academic or calendar year used
year_calendar = False
#selected_coutries = ['AT','BE','CH','CZ','DE','DK','ES','FI','FR','IE','IT','LU','NL','NO','PL','PT','SE','UK']

# Select countries
selected_coutries = ['AT','BE','CH','DE','DK','FR','IT','LU','NL']
selected_base_year = '2010-2011'

"""
# Select base year and iteration years
if year_calendar == True:  
    selected_base_year = 2008
    selected_years = [y for y in range(1982, 2019)]
    
if year_calendar == False:
    selected_base_year = "2008-2009"
    selected_years = [
        "1990-1991",
        "1991-1992",
        "1992-1993",
        "1993-1994",
        "1994-1995",
        "1995-1996",
        "1996-1997",
        "1997-1998",
        "1998-1999",
        "1999-2000",
        "2000-2001",
        "2001-2002",
        "2002-2003",
        "2003-2004",
        "2004-2005",
        "2005-2006",
        "2006-2007",
        "2007-2008",
        "2008-2009",
        "2009-2010",
        "2010-2011"
        ]
"""

#%% Create base data

# Get data - execute function
base_ts = create_timeseries_input(data_dict,selected_base_year,selected_coutries,year_calendar,period_length)

# Export to csv
base_ts.to_csv(os.path.join("project_files","data_input","data_input_wide","0_Core","time_data_upload.csv"))

####################################################################################################
####################################################################################################

"""

#%% Iteration data function ########################################################################

def create_iteration_data(dict_df, years, selected_countries, hydro_constant,year_calendar=True,
                          period_length=1):
    
    iteration_data_dict = {}
    
    for y in years:
        
        # Filter data
        if period_length == 1:

            if year_calendar == True:
                sel_demand    = dict_df["demand"].query("year == @y & n == @selected_countries") 
                sel_pv        = dict_df["pv"].query("year == @y & n == @selected_countries")
                sel_wind_on   = dict_df["wind_on"].query("year == @y & n == @selected_countries") 
                sel_wind_off  = dict_df["wind_off"].query("year == @y & n == @selected_countries") 
                sel_rsvr      = dict_df["rsvr"].query("year == @y & n == @selected_countries") 
                sel_phs       = dict_df["phs_open"].query("year == @y & n == @selected_countries") 
                sel_ror       = dict_df["ror"].query("year == @y & n == @selected_countries") 
            if year_calendar == False:
                sel_demand    = dict_df["demand"].query("year_ac == @y & n == @selected_countries") 
                sel_pv        = dict_df["pv"].query("year_ac == @y & n == @selected_countries")
                sel_wind_on   = dict_df["wind_on"].query("year_ac == @y & n == @selected_countries") 
                sel_wind_off  = dict_df["wind_off"].query("year_ac == @y & n == @selected_countries") 
                sel_rsvr      = dict_df["rsvr"].query("year_ac == @y & n == @selected_countries") 
                sel_phs       = dict_df["phs_open"].query("year_ac == @y & n == @selected_countries") 
                sel_ror       = dict_df["ror"].query("year_ac == @y & n == @selected_countries") 
    
        if period_length > 1:
        
            years_sel = [y,y+1]
        
            sel_demand    = dict_df["demand"].query("year == @years_sel & n == @selected_countries") 
            sel_pv        = dict_df["pv"].query("year == @years_sel & n == @selected_countries")
            sel_wind_on   = dict_df["wind_on"].query("year == @years_sel & n == @selected_countries") 
            sel_wind_off  = dict_df["wind_off"].query("year == @years_sel & n == @selected_countries") 
            sel_rsvr      = dict_df["rsvr"].query("year == @years_sel & n == @selected_countries") 
            sel_phs       = dict_df["phs_open"].query("year == @years_sel & n == @selected_countries") 
            sel_ror       = dict_df["ror"].query("year == @years_sel & n == @selected_countries") 

            # Add multi-year index
            sel_demand["h_multi"]   = sel_demand.groupby(["n"]).cumcount()+1
            sel_pv["h_multi"]       = sel_pv.groupby(["n"]).cumcount()+1  
            sel_wind_on["h_multi"]  = sel_wind_on.groupby(["n"]).cumcount()+1
            sel_wind_off["h_multi"] = sel_wind_off.groupby(["n"]).cumcount()+1
            sel_rsvr["h_multi"]     = sel_rsvr.groupby(["n"]).cumcount()+1
            sel_phs["h_multi"]      = sel_phs.groupby(["n"]).cumcount()+1
            sel_ror["h_multi"]      = sel_ror.groupby(["n"]).cumcount()+1

        else:
            raise TypeError("Period length not properly defined")
                
        # Add identifier
        sel_demand['identifier']   = 'load_'        + sel_demand['n']
        sel_pv['identifier']       = 'pv_'          + sel_pv['n']
        sel_wind_on['identifier']  = 'wind_on_'     + sel_wind_on['n']
        sel_wind_off['identifier'] = 'wind_off_'    + sel_wind_off['n']
        sel_rsvr['identifier']     = 'rsvr_'        + sel_rsvr['n']
        sel_phs['identifier']      = 'phs_open_'    + sel_phs['n']
        sel_ror['identifier']      = 'ror_'         + sel_ror['n']
        
        # Add parameter
        sel_demand['parameter']   = "d('"           + sel_demand['n'] + "',h)"
        sel_pv['parameter']       = "phi_res('"     + sel_pv['n'] + "','pv',h)"
        sel_wind_on['parameter']  = "phi_res('"     + sel_wind_on['n'] + "','wind_on',h)"
        sel_wind_off['parameter'] = "phi_res('"     + sel_wind_off['n'] + "','wind_off',h)"
        sel_rsvr['parameter']     = "rsvr_in('"     + sel_rsvr['n'] + "','rsvr',h)"
        sel_phs['parameter']      = "sto_flow_in('" + sel_phs['n'] + "','phs_open',h)"
        sel_ror['parameter']      = "phi_res('"     + sel_ror['n'] + "','ror',h)"    
        
        # Concat DFs
        
        if hydro_constant == True:   
            it_data_year = pd.concat([sel_pv,sel_wind_on,sel_wind_off], ignore_index = True) #sel_demand
        
        else:        
            it_data_year = pd.concat([sel_pv,sel_wind_on,sel_wind_off,sel_rsvr,sel_phs,
                                    sel_ror], ignore_index = True) #sel_demand
        
        # choose correct hour index
        if period_length > 1:
            index_hour = "h_multi"
        else:
            if year_calendar == True:
                index_hour = "h"
            if year_calendar == False:
                index_hour = "h2"  
        
        it_data_year = it_data_year[['parameter','identifier',index_hour,'value']] 
        
        # Add scenario
        it_data_year["scenario"] = "y"+str(y)
                
        sel_ts = it_data_year.pivot_table(index = index_hour, values = 'value',
                                        columns = ['parameter','identifier','scenario'])
        
        sel_ts.index.name = None
        sel_ts.insert(value=range(1,8760*period_length+1),column='t',loc=0)
        nr_digits   = len(str(max(sel_ts['t']))) 
        sel_ts['h'] = 't' + sel_ts['t'].astype(str).str.zfill(nr_digits)
        sel_ts = sel_ts.set_index('h')
        sel_ts.index.name = None
        
        # Drop column t
        sel_ts = sel_ts.drop("t", axis=1)
        
        iteration_data_dict[y] = sel_ts
    
    return iteration_data_dict

#%%

# if period_length > 1:
#     filename = "timeseries_input_multi.xlsx"
# 
# else:
#     if year_calendar == True:  
#         filename = "timeseries_input_cal.xlsx"
#     if year_calendar == False:
#         filename = "timeseries_input_aca.xlsx"
# 
# # Write to Excel, replace old 'basic' sheet
# with pd.ExcelWriter(os.path.join('project_files','data_input',filename), mode='a', if_sheet_exists = 'replace') as writer:  
#     base_ts.to_excel(writer, sheet_name='basic_import', startrow=5, index=False, header=False)

#%% Create iteration data ##########################################################################

# Get data
iteration_data_dict = create_iteration_data(data_dict, selected_years, selected_coutries,
                                            hydro_constant=True,
                                            year_calendar=year_calendar,
                                            period_length=period_length)

# Define Python part
py = {'symbol': ['identifer', 'scenario', 'iter_data', 'h'],
        'sheet_name': ['scenario','scenario','scenario','scenario'], 
        'startcell': ['B4','B5','A4','A6'], 
        'rdim': [0,0,1,1], 
        'cdim':[1,1,2,0],
        'type':['set','set','par','set']
        }

py = pd.DataFrame(data=py)

#%% Write to excel

print("Start saving")
print("----------")

for year in selected_years:
    print(year)
    print("----------")   
    file_path = "project_files/iterationfiles"
    file_name = "iteration_data_" + str(year) + ".xlsx"
    df_to_write = iteration_data_dict[year]
    
    with pd.ExcelWriter(os.path.join(file_path,file_name), mode='w') as writer:
        py.to_excel(writer, sheet_name = 'py', startrow = 0, header = True, index = False)
        df_to_write.to_excel(writer, sheet_name='scenario', startrow = 2, header = True, index = True)

    # delete buggy empty row
    wb = openpyxl.load_workbook(os.path.join(file_path, file_name))
    sheet = wb['scenario']
    sheet.delete_rows(6, 1)
    wb.save(os.path.join(file_path, file_name))

"""

#%% ################################################################################################