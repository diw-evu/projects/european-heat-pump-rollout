
********************************************************************************
$ontext
The Dispatch and Investment Evaluation Tool with Endogenous Renewables (DIETER).
Version 1.5.0, April 2021.
Written by Alexander Zerrahn, Wolf-Peter Schill, and Fabian St�ckl.
This work is licensed under the MIT License (MIT).
For more information on this license, visit http://opensource.org/licenses/mit-license.php.
Whenever you use this code, please refer to http://www.diw.de/dieter.
We are happy to receive feedback under wschill@diw.de.
$offtext
********************************************************************************

$setglobal DIETERgms ""

%DIETERgms%$setglobal MODELDIR %py_modeldir%

**************************
***** GLOBAL OPTIONS *****
**************************

%DIETERgms%$include "%MODELDIR%dieterpy_1_globalopt.gms"

%DIETERgms%$ontext

* Set star to skip Excel upload and load data from gdx
$setglobal skip_Excel ""

* Germany only - also adjust Excel inputs! by adding a empty column after DE node in spatial sheet
$setglobal GER_only "*"

* Set star to activate options
$setglobal DSM ""

$setglobal prosumage ""

$setglobal heat ""

$setglobal EV ""
$setglobal EV_EXOG ""

$setglobal P2H2P_parsimonious ""
* Set star to activate extensive P2H2 module (The two sub-modules: "re-conversion" and "H2 production and distribution for traffic" are controlled in more detail via the data_input Excel file!)
$setglobal P2H2_extensive ""
$setglobal Simplified_transporation ""
$setglobal Time_consuming_transportation ""

* Set star for no crossover to speed up calculation time by skipping crossover in LP solver
$setglobal no_crossover ""

$ontext
$offtext
* This closes the DIETERgms

********************************************************************************

* Sanity checks

$if "%EV%" == "" $if "%EV_EXOG%" == "*" $abort Switch on EV! ;
$if "%P2H2P_parsimonious%" == "*" $if %P2H2_extensive%" == "*" "$abort Choose one but only one of the hydrogen modules - either parsimonious or extensive.;
$if "%P2H2_extensive%" == "*" $if "%Simplified_transporation%" == "%Time_consuming_transportation%" $abort Choose one but only one type of transportation - either simplified or time consuming! ;

********************************************************************************

**************************
***** SOLVER OPTIONS *****
**************************

options
lp = CPLEX
optcr = 0.00
reslim = 10000000
limrow = 0
limcol = 0
dispwidth = 15
solprint = off
sysout = off
;

********************************************************************************

**************************
***** Dataload *****
**************************

%DIETERgms%$ontext
* Inclusion of scenario
$include "dataload.gms"
$ontext
$offtext

%DIETERgms%$include "%MODELDIR%dataload.gms"

********************************************************************************

%DIETERgms%$ontext
*************************************
***** Features for single nodes *****
*************************************

Set
features /dsm, ev, prosumage, heat, P2H2P_parsimonious, P2H2_extensive/
;

Table
feat_node(features,n)
                    DE
%DSM%$ontext
dsm                 0
$ontext
$offtext
%DIETERgms%$ontext
%ev%$ontext
ev                  0
$ontext
$offtext
%DIETERgms%$ontext
%prosumage%$ontext
prosumage           0
$ontext
$offtext
%DIETERgms%$ontext
%heat%$ontext
heat                0
$ontext
$offtext
%DIETERgms%$ontext
%P2H2P_parsimonious%$ontext
P2H2P_parsimonious                0
$ontext
$offtext
%DIETERgms%$ontext
%P2H2_extensive%$ontext
P2H2_extensive                0
$ontext
$offtext
;

$ontext
$offtext
* This closes the DIETERgms


***** Reduce problem size by removing entries of nodes not included in the feat_node parameter  *****

%DSM%$ontext
m_dsm_cu(n,dsm_curt)$(feat_node('dsm',n) = 0) = 0 ;
m_dsm_shift(n,dsm_shift)$(feat_node('dsm',n) = 0) = 0 ;
$ontext
$offtext

%prosumage%$ontext
m_res_pro(n,res)$(feat_node('prosumage',n) = 0) = 0 ;
m_sto_pro_e(n,sto)$(feat_node('prosumage',n) = 0) = 0 ;
m_sto_pro_p(n,sto)$(feat_node('prosumage',n) = 0) = 0 ;
$ontext
$offtext

%heat%$ontext
*dh(n,bu,ch,h)$(feat_node('heat',n) = 0) = 0 ;
*d_dhw(n,bu,ch,h)$(feat_node('heat',n) = 0) = 0 ;
heat_demand(n,heat_building,heat_sink,h)$(feat_node('heat',n) = 0) = 0 ;
$ontext
$offtext

%P2H2P_parsimonious%$ontext
d_h2_parsimonious(n,h)$(feat_node('hydrogen_parsimonious',n) = 0) = 0 ;
$ontext
$offtext

Set
map_n_tech(n,tech)
map_n_sto(n,sto)
map_n_dsm(n,dsm)
map_n_ev(n,ev)
map_l(l)
map_n_sto_pro(n,sto)
map_n_res_pro(n,tech)
map_n_h2_ely(n,h2_ely)
map_n_h2_sto(n,h2_sto)
map_n_h2_recon(n,h2_recon)

map_n_heat(n,heat_building,heat_sink,heat_tech)

;

map_n_tech(n,tech)          = yes$max_p(n,tech) ;                
map_n_sto(n,sto)            = yes$max_sto_e(n,sto) ;
map_n_dsm(n,dsm_curt)       = yes$m_dsm_cu(n,dsm_curt) ;
map_n_dsm(n,dsm_shift)      = yes$m_dsm_shift(n,dsm_shift) ;
map_n_ev(n,ev)              = yes$phi_ev(n,ev) ;
map_l(l)                    = yes$max_ntc_ex(l) + yes$max_ntc_im(l) ;
map_n_sto_pro(n,sto)        = yes$m_sto_pro_p(n,sto) ;
map_n_res_pro(n,res)        = yes$m_res_pro(n,res) ;
map_n_h2_ely(n,h2_ely)      = yes$max_h2_ely_p(n,h2_ely) ;
map_n_h2_sto(n,h2_sto)      = yes$max_h2_sto_e(n,h2_sto) ;
map_n_h2_recon(n,h2_recon)  = yes$max_h2_recon_p(n,h2_recon) ;

map_n_heat(n,heat_building,heat_sink,heat_tech) = heat_share(n,heat_building,heat_sink,heat_tech);

;

* No interconnections between non-adjacent or nonuploaded nodes
max_ntc_ex(l)$( smax(n,inc(n,l)) = 0 OR smin(n,inc(n,l)) = 0 ) = 0 ;
max_ntc_im(l)$( smax(n,inc(n,l)) = 0 OR smin(n,inc(n,l)) = 0 ) = 0 ;

********************************************************************************
***** Model *****
********************************************************************************

%DIETERgms%$ontext
* Inclusion of scenario
$include "model.gms"
$ontext
$offtext

%DIETERgms%$include "%MODELDIR%model.gms"

********************************************************************************
***** Options, fixings, report preparation *****
********************************************************************************

%DIETERgms%$include "%MODELDIR%dieterpy_6_solveropt.gms"

%DIETERgms%$ontext

* Solver options
$onecho > cplex.opt
lpmethod 4
threads 0
SolutionType 2
barepcomp 1e-8
datacheck 2
quality 1
predual -1
$offecho

%no_crossover%$ontext
$onecho > cplex.opt
lpmethod 4
threads 0
barcrossalg -1
SolutionType 2
barepcomp 1e-8
datacheck 2
quality 1
predual -1
$offecho
$ontext
$offtext
;

$ontext
$offtext
* This closes the DIETERgms

dieter.OptFile = 1;
dieter.holdFixed = 1;


%DIETERgms%$ontext
* Inclusion of scenario
$include "scenario.gms"
$ontext
$offtext

%DIETERgms%$ontext
* Inclusion of scenario
$include "fix.gms"
$ontext
$offtext


%DIETERgms%$include "%MODELDIR%fix.gms"
%DIETERgms%$include "%MODELDIR%dieterpy_7_custom.gms"

********************************************************************************
***** Solve *****
********************************************************************************
*Capture model status and solver status
%DIETERgms%Scalar ss ms;

%DIETERgms%$ontext
solve DIETER using lp min Z;

execute_unload "results.gdx";
$ontext
$offtext
