#%% Load packages

import pandas as pd
import numpy as np

#%% Define functions

def pre_process_function(input_data,output_name,end_date):
    
    # Long-to-wide
    input_data_wide = input_data.pivot(index="utc_timestamp",columns=["country","attribute"],values="data").reset_index()
    
    # Proper time index
    input_data_wide["utc_timestamp"] = pd.to_datetime(input_data_wide["utc_timestamp"]).dt.tz_convert(None)

    # Create new hourly data data

    hours = pd.Series(pd.date_range(
        start=pd.to_datetime("2008-01-01 00:00:00"),
        end=pd.to_datetime(end_date), freq='h'))

    hours = pd.concat([hours], axis=0)

    # Create df with only an hourly column
    df_heat_wide_hours = pd.DataFrame({"h": hours})
    df_heat_wide_hours.columns = pd.MultiIndex.from_tuples([('h','')])

    # Drop 29/2
    df_heat_wide_hours = df_heat_wide_hours.loc[
        ~(df_heat_wide_hours['h'].dt.month.eq(2) & df_heat_wide_hours['h'].dt.day.eq(29))].reset_index(drop=True)

    # Merge with input data
    df_wide = df_heat_wide_hours.merge(
        input_data_wide, how="left", left_on = "h", right_on = "utc_timestamp")

    # Drop UTC column
    df_wide = df_wide.drop(['utc_timestamp'], axis=1)
    
    # Fill NAs
    df_wide = df_wide.drop(["h"], axis=1).interpolate(
        method='linear', axis=0)

    # Merge back
    df_wide2 = df_heat_wide_hours.join(df_wide)

    # Add direct heating
    list_countries = input_data['country'].unique()
    for n in list_countries:
        df_wide2.insert(0, (n, 'direct_space'), [1] * df_wide2.shape[0])
        df_wide2.insert(0, (n, 'direct_water'), [1] * df_wide2.shape[0])

    df_wide2 = df_wide2.sort_index(axis=1)
    df_wide2.insert(0, ('h',''), df_wide2.pop(('h','')))
        
    # Wide-to-long (melt)
    df_long = pd.melt(df_wide2, id_vars="h")
    
    # Rename
    df_long = df_long.rename(columns = {"variable_0":"n", "variable_1":"type", "h":"hour"})

    # Create hourly index
    df_long["year"] = df_long["hour"].dt.year
    df_long["h"]    = df_long.groupby(["n","type","year"]).cumcount()+1

    # Drop not needed columns
    df_long = df_long.drop("hour", axis = 1)

    #Save
    df_long.to_feather(os.path.join("data","heat",output_name))
    
    return df_long

def export_heat_data(input_data, data_type, selected_year, selected_countries, year_type):
    
    if data_type == "hd":
        selected_variables = ['space_COM', 'space_MFH', 'space_SFH',
                        'water_COM', 'water_MFH', 'water_SFH']
        input_data = input_data.query("year == @selected_year & n == @selected_countries & type == @selected_variables")
    if data_type == "cop":
        input_data = input_data.query("year == @selected_year & n == @selected_countries")

    # Split columns
    if data_type ==  "hd":
        input_data[['heat_sink','heat_building']] = input_data['type'].str.split('_', expand=True)
        
    if data_type ==  "cop":
        input_data[['heat_type_aux','heat_sink']] = input_data['type'].str.split('_', expand=True)

    # Rename values
    if data_type == "hd":
        input_data = input_data.replace({"COM": "com", "SFH":"sfh", "MFH": "mfh"})
    if data_type == "cop":
        input_data['heat_sink'] = input_data['heat_sink'].replace({"floor": "space", "radiator":"space"})
        input_data['type']      = input_data['type'].replace({
            'ASHP_floor'   :'hp_air_floor',
            'ASHP_radiator': 'hp_air_radiator',
            'ASHP_water'   : 'hp_air_water',
            'GSHP_floor'   : 'hp_ground_floor',
            'GSHP_radiator': 'hp_ground_radiator',
            'GSHP_water'   : 'hp_ground_water',
            'WSHP_floor'   : 'hp_water_floor',
            'WSHP_radiator': 'hp_water_radiator',
            'WSHP_water'   : 'hp_water_water',
            'direct_space' : 'direct',
            'direct_water' : 'direct'
            })
    
    # Create alternative 
    input_data['year'] = input_data['year'].astype(int)
    input_data['h']    = input_data['h'].astype(int)

    # Define alternative year 
    input_data['year2']   = np.where(input_data['h']<=4344, input_data['year'] - 1, input_data['year'])
    input_data['year_ac'] = input_data['year2'].map(str) + "-" +(input_data['year2']+1).map(str)
    input_data.drop(columns=['year2'], inplace = True)
    
    # Define alternative h-index (July-June)
    input_data['h2'] = input_data['h'] - 4344
    input_data['h2'] = np.where(input_data['h2'] <= 0, input_data['h2'] + 8760, input_data['h2'])
    
    # Create new index
    if year_type == "jan-dec":
        input_data['h_new'] = 't' + input_data['h'].astype(str).str.zfill(4)
    if year_type == "jul-jun":
        input_data['h_new'] = 't' + input_data['h2'].astype(str).str.zfill(4)
    else:
        raise Exception("Define type year") 
        
    # Choose correct columns
    if data_type == "hd":
        input_data      = input_data[["h_new","n","heat_building","heat_sink","value"]].reset_index(drop=True)
        input_data_wide = input_data.pivot(index=['n','heat_building','heat_sink'], columns='h_new', values='value')
    
    if data_type == "cop":
        input_data      = input_data[["h_new","n","type","heat_sink","value"]].reset_index(drop=True)
        input_data      = input_data.rename(columns={"type":"heat_tech"})
        input_data_wide = input_data.pivot(index=['n','heat_sink','heat_tech'], columns='h_new', values='value')
    
    # Save
    if data_type == "hd":
        input_data_wide.to_csv(os.path.join("project_files","data_input","data_input_wide","1_Heat","heat_demand.csv"))
    if data_type == "cop":
        input_data_wide.to_csv(os.path.join("project_files","data_input","data_input_wide","1_Heat","heat_cop.csv"))

#%% Import data ####################################################################################

pre_process_data = False

if pre_process_data:
    df_heat_stacked = pd.read_csv(os.path.join("data","heat","when2heat_stacked.csv"), sep=",", decimal=".")
    df_cop_stacked  = df_heat_stacked.query("variable == 'COP'")
    df_hd_stacked   = df_heat_stacked.query("variable == 'heat_demand'")
    
    cop = pre_process_function(df_cop_stacked,"cop.feather","2022-12-31 23:00:00")
    hd  = pre_process_function(df_hd_stacked,"hd.feather","2015-12-31 23:00:00")

else:
    cop = pd.read_feather(os.path.join("data","heat","cop.feather"))
    hd  = pd.read_feather(os.path.join("data","heat","hd.feather"))

#%% Select data ####################################################################################

selected_year      = 2010
selected_countries = ["AT","BE","CH","DE","DK","FR","IT","LU","NL"]

#%% Write data #####################################################################################

export_heat_data(hd,  "hd",  selected_year, selected_countries, "jul-jun")
export_heat_data(cop, "cop", selected_year, selected_countries, "jul-jun")

#%% ################################################################################################