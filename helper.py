import pandas as pd
import numpy as np

#%% Import data ####################################################################################

#!! using 2025 for demand, instead of 2030
demand2030       = pd.read_parquet("data/eraa2021/PECD-country-demand_national_estimates-2025.parquet")
#!!

pv2030           = pd.read_parquet("data/eraa2021/PECD-2021.3-country-LFSolarPV-2030.parquet")
windon2030       = pd.read_parquet("data/eraa2021/PECD-2021.3-country-Onshore-2030.parquet")
windoff2030      = pd.read_parquet("data/eraa2021/PECD-2021.3-country-Offshore-2030.parquet")
ror2030_gen      = pd.read_parquet("data/eraa2021/PECD_EERA2021_ROR_2030_country_gen.parquet")
hydro2030_inflow = pd.read_parquet("data/eraa2021/PECD_EERA2021_reservoir_pumping_2030_country_inflow.parquet")

ror2030_cap      = pd.read_csv("data/eraa2021/PECD_EERA2021_ROR_2030_country_table.csv")

print("Done: read files")

#%% Create proper time index #######################################################################

df_list = [pv2030,windon2030,windoff2030,demand2030]

for df in df_list:
    df["year"] = df["year"].astype(int)
    df["h"] = df.groupby(["country","year"]).cumcount() + 1

print("Done: create time index")

#%% RoR ############################################################################################

# Create hourly data
ror2030_gen            = ror2030_gen.loc[ror2030_gen.index.repeat(24)].reset_index(drop=True)
ror2030_gen["gen_GWh"] = ror2030_gen["gen_GWh"] / 24
ror2030_gen["h"]       = ror2030_gen.groupby(["country","year"]).cumcount() + 1

# Convert to MWh
ror2030_gen["gen_MWh"] = ror2030_gen["gen_GWh"] * 1000

#%% Create capacity factor
ror2030_merge       = pd.merge(ror2030_gen,ror2030_cap,how="left",on="country")
ror2030_merge["cf"] = ror2030_merge["gen_MWh"] / ror2030_merge["cap_MW"]

# limit cf to 1
ror2030_merge["cf"] = np.where( ror2030_merge["cf"] > 1, 1, ror2030_merge["cf"] )
ror2030_in = ror2030_merge

#%% Hydro ##########################################################################################

hydro2030_inflow_w53 = hydro2030_inflow.query("Week == 53")

# Remove week 53
hydro2030_inflow = hydro2030_inflow.query("Week != 53")

# Create hourly data
hydro2030_inflow  = hydro2030_inflow.loc[hydro2030_inflow.index.repeat(168)].reset_index(drop=True)
hydro2030_inflow["inflow_GWh"] = hydro2030_inflow["inflow_GWh"] / 168

# Special treatment of week 53
hydro2030_inflow_w53["inflow_GWh"] = hydro2030_inflow_w53["inflow_GWh"] / 168
hydro2030_inflow_w53               = hydro2030_inflow_w53.loc[hydro2030_inflow_w53.index.repeat(24)].reset_index(drop=True)

# Concat data
hydro2030_inflow = pd.concat([hydro2030_inflow,hydro2030_inflow_w53]).sort_values(
    by=["country","technology","year","Week"])

# Create h index
hydro2030_inflow["h"]          = hydro2030_inflow.groupby(["country","year","technology"]).cumcount() + 1

# Convert to MWh
hydro2030_inflow["inflow_MWh"] = hydro2030_inflow["inflow_GWh"] * 1000

#%% Create hydro files #############################################################################

phs_open2030 = hydro2030_inflow.query("technology == 'pumped_open'").reset_index(drop=True)
rsvr2030     = hydro2030_inflow.query("technology == 'reservoir'").reset_index(drop=True)

print("Done: define hydro files")

#%% Rename and reorder columns #####################################################################

pv2030           = pv2030[["country","year","h","cf"]].rename(columns={"cf":"value","country":"n"})
windon2030       = windon2030[["country","year","h","cf"]].rename(columns={"cf":"value","country":"n"})
windoff2030      = windoff2030[["country","year","h","cf"]].rename(columns={"cf":"value","country":"n"})
demand2030       = demand2030[["country","year","h","dem_MW"]].rename(columns={"dem_MW":"value","country":"n"})
ror2030_in       = ror2030_in[["country","year","h","cf"]].rename(columns={"cf":"value","country":"n"})
phs_open2030     = phs_open2030[["country","year","h","inflow_MWh"]].rename(columns={"inflow_MWh":"value","country":"n"})
rsvr2030         = rsvr2030[["country","year","h","inflow_MWh"]].rename(columns={"inflow_MWh":"value","country":"n"})

#%% Set datatypes

df_list = [pv2030,windon2030,windoff2030,demand2030,ror2030_in,phs_open2030,rsvr2030]

for dafr in df_list:
    dafr['year'] = dafr['year'].astype(int)
    dafr['h']    = dafr['h'].astype(int)

    # Define alternative year 
    dafr['year2']   = np.where(dafr['h']<=4344, dafr['year'] - 1, dafr['year'])
    dafr['year_ac'] = dafr['year2'].map(str) + "-" +(dafr['year2']+1).map(str)
    dafr.drop(columns=['year2'], inplace = True)
    
    # Define alternative h-index (July-June)
    dafr['h2'] = dafr['h'] - 4344

    dafr['h2'] = np.where(dafr['h2'] <= 0, dafr['h2'] + 8760, dafr['h2'])

#%% Create dict

data_dict = {
    "demand"   : demand2030,
    "pv"       : pv2030,
    "wind_on"  : windon2030,
    "wind_off" : windoff2030,
    "rsvr"     : rsvr2030,
    "phs_open" : phs_open2030,
    "ror"      : ror2030_in
}

print("Done: create dicts")

#%% Export to time_series_input function ###########################################################

def create_timeseries_input(dict_df, selected_year,selected_countries,year_calendar=True,period_length=1):
    
    if period_length == 1:

        # Filter data
        if year_calendar == True:
            sel_demand    = dict_df["demand"].query("year == @selected_year & n == @selected_countries") 
            sel_pv        = dict_df["pv"].query("year == @selected_year & n == @selected_countries")
            sel_wind_on   = dict_df["wind_on"].query("year == @selected_year & n == @selected_countries") 
            sel_wind_off  = dict_df["wind_off"].query("year == @selected_year & n == @selected_countries") 
            sel_rsvr      = dict_df["rsvr"].query("year == @selected_year & n == @selected_countries") 
            sel_phs       = dict_df["phs_open"].query("year == @selected_year & n == @selected_countries") 
            sel_ror       = dict_df["ror"].query("year == @selected_year & n == @selected_countries") 
        if year_calendar == False:
            sel_demand    = dict_df["demand"].query("year_ac == @selected_year & n == @selected_countries") 
            sel_pv        = dict_df["pv"].query("year_ac == @selected_year & n == @selected_countries")
            sel_wind_on   = dict_df["wind_on"].query("year_ac == @selected_year & n == @selected_countries") 
            sel_wind_off  = dict_df["wind_off"].query("year_ac == @selected_year & n == @selected_countries") 
            sel_rsvr      = dict_df["rsvr"].query("year_ac == @selected_year & n == @selected_countries") 
            sel_phs       = dict_df["phs_open"].query("year_ac == @selected_year & n == @selected_countries") 
            sel_ror       = dict_df["ror"].query("year_ac == @selected_year & n == @selected_countries") 
    
    elif period_length > 1:
        
        years_sel = [selected_year,selected_year+1]
        
        sel_demand    = dict_df["demand"].query("year == @years_sel & n == @selected_countries") 
        sel_pv        = dict_df["pv"].query("year == @years_sel & n == @selected_countries")
        sel_wind_on   = dict_df["wind_on"].query("year == @years_sel & n == @selected_countries") 
        sel_wind_off  = dict_df["wind_off"].query("year == @years_sel & n == @selected_countries") 
        sel_rsvr      = dict_df["rsvr"].query("year == @years_sel & n == @selected_countries") 
        sel_phs       = dict_df["phs_open"].query("year == @years_sel & n == @selected_countries") 
        sel_ror       = dict_df["ror"].query("year == @years_sel & n == @selected_countries") 

        # Add multi-year index
        sel_demand["h_multi"]   = sel_demand.groupby(["n"]).cumcount()+1
        sel_pv["h_multi"]       = sel_pv.groupby(["n"]).cumcount()+1  
        sel_wind_on["h_multi"]  = sel_wind_on.groupby(["n"]).cumcount()+1
        sel_wind_off["h_multi"] = sel_wind_off.groupby(["n"]).cumcount()+1
        sel_rsvr["h_multi"]     = sel_rsvr.groupby(["n"]).cumcount()+1
        sel_phs["h_multi"]      = sel_phs.groupby(["n"]).cumcount()+1
        sel_ror["h_multi"]      = sel_ror.groupby(["n"]).cumcount()+1

    else:
        raise TypeError("Period length not properly defined")
        
    # Add identifier
    sel_demand['identifier']   = 'demand'
    sel_pv['identifier']       = 'pv'
    sel_wind_on['identifier']  = 'wind_on'
    sel_wind_off['identifier'] = 'wind_off'
    sel_rsvr['identifier']     = 'rsvr'
    sel_phs['identifier']      = 'phs_open'
    sel_ror['identifier']      = 'ror'
    
    # Concat DFs
    sel_long = pd.concat([sel_demand, sel_pv, sel_wind_on, sel_wind_off, sel_ror, sel_phs, 
                        sel_rsvr], ignore_index = True)
    
    # choose correct hourly index
    
    if period_length > 1:
        index_hour = "h_multi"
    else:
        if year_calendar == True:
            index_hour = "h"
        if year_calendar == False:
            index_hour = "h2"   
    
    nr_digits   = len(str(max(sel_long[index_hour])))
    sel_long[index_hour] = 't' + sel_long[index_hour].astype(str).str.zfill(nr_digits)
    
    sel_long = sel_long.rename(columns={'identifier':'headers_time'})
    
    sel_ts = sel_long.pivot(index=['headers_time','n'], columns=index_hour, values='value')
    
    return sel_ts